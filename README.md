# READ ME #

## dummyText_NCM class ##

the dummyText_NCM module is designed to generate various Lorem Ipsum style text.  you can either generate words, sentences or paragraphs.  below is a list of the dummy texts we can generate.  more to come soon(tm).

* Lorem Ipsum
* Meats
* Breads
* Cheeses
* Seafood
* Dogs
* Vegetables
* Fruit

### testing ###
currently there is no testing.  testing will come soon(tm).

### optimization ###
currently the methods are not optimized.  All methods are using the first try to complete the method over using the best method.  Optimizations will come in the future.

### sample code ###
there is a sample program that generates dummy text using this module.

### version history ###
* 2019-05-26 - added Vegetables, Fruit & Dogs.  also converted from binary format to VCP format.
* 2018-02-27 - added Lorem Ipsum, Bread, Meats, Seafood & Cheeses
* 2018-02-24 - initial commit (for public sharing).
