#tag Class
Protected Class dummyTextNCM_UnitTests
Inherits TestGroup
	#tag Method, Flags = &h0
		Sub boolean_wordlistDT__001_Test()
		  const strWords      as string = "aish merahrah;ajdov kruh;anadama bread;anpan;appam;arepa;baba;bagel;baguette;bakrakhani;bakshalu;balep korkun;bammy;banana bread;bannok;bara birth;barbari bread;barm cake;barmbrack;barotta;bastone;baziama;bazin;beer bread;belgian waffle;bhakri;biaa;bialy;bing;biscuit;black bread;bobbatlu;bolani;bolo do caco;borlengo;borodinsky;boule;bramboráček bread roll;breadstick;brioche;broa;brown bread;bublik;buckwheat;bun;canadian white;carrot bread;Česnica;challah;chapati;chickpea bread;cholermüs;christmas wafer;ciabatta;coppia ferrarese;cornbread;cottage load;cracker;crêpe;crisp bread;crispy bread;croutons;crumpet;cuban bread;curry bread;damper;dampfnudel;dorayaki;dosa;dry bread;eggette;english muffin;fari;filone;flatbread;flatbrød;flatkaka;focaccia;fruit bread;green onion pancake;hallulla;hard dough bread;hardebrood;hardtack;himbasha;hoecake;injera;johnnycake;ka'ak;kalach kaya toast;khakhra;khanom bueang;khubz;kiﬂi;kisra;kulcha;laffa;lagana;lahoh;laobing;lavash;leavened;lefse;malooga;mantou;markook;marraqueta;massa sovada;matzo;melonpan;miche;michetta;mollete;montrealStyle bagel;multigrain bread;naan;ngome;obbatu;obwarzanek krakowski;pain d'epi;pain de mie;pan dulce;pana carasau;pana ticinese;panbrioche;pancake;pancarré;pandesal;pandoro;pane di altamura;panettone;panfocaccia;papad;papadum;paratha;parotta;paximathia;penia;pex bread;piadina;pikelet;pistolet;pita;pizza;pogača;potato bread;potato pancake;potbrood;pretzel;proja;pumpernickel;puran poli;qistibi;quick bread;rewena bread;rice bread;roti;rugbrød;rumali roti;ryainjun;rye bread;rye;sacramental bread;saj bread;saltRising bread;sangak;scone;sgabeo;shirmal;shoti;soboro bread;soda bread;sourdough bread;sourdough;speckendick;spelt bread;sprouted bread;sweet bread;sweet bun;taboon bread;taftan;tandoor bread;teacake;texas toast;tiger bread;toasted bread;tortilla;tsoureki;ttongppang;tunnbröd;vánočka;vienna bread;waffle;wheat;white bread;whole wheat bread;yeast;yufka;zopf;zwieback"
		  dim   strExpected() as string = strWords.split( ";" )
		  dim   strResults()  as string = wordListDT( "Bread" )
		  strExpected.sort
		  strResults.sort
		  Assert.areEqual( strExpected,strResults )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub boolean_wordlistDT__002_Test()
		  const strWords      as string = "abaza;abertam;adadero;Ädelost;adobera;adygheysky;akkawi;altaysky;anari;añejo;antep;anthotyros;areesh;armola;arnavut;atleet;aura;ayibe;bachensteiner;baladi;balıkesir;bandel;basket;belo sirenje;bergenost bergkäse;beyaz peynir;bilozhar;blå gotland;bohinc jože;brânzǎ de burduf;brânză de suhaia;brânză de vaci;brick;brimsen brunost;brussels;bryndza;bukovinskyi;byaslag;Çamur;caravane;caş;caşcaval;castelo branco;catupiry;Çerkez füme;chanco;chaqueño;charkassiye;chechil;cheddar;cheese curds;cherni vit;chhena;chhurpi;chiapas;chihuahua;chimay cheeses;chloro;chubut;chura kampo;chura loenpa;chyorny altai;circassian;civil;Çökelek;colby;colbyJack;colorado blackie;Çömlek;cotija;cottage cream cheese;crema;cremoso;creole cream cheese;criollo;cuajada;cup;dahi chhena;danbo;dangke;danilovsky;danish blue;dil;dimsi;dobrodar;domiati;dorogobuzhsky;dorozhny;dziugas;edirne;eesti juust;enredo;esrom;ezine;farmer farmer;feta;flower of rajya;formaela;fynbo;galotyri;gamalost;gauda;gbenja;geilost;gelundener käse;golandsky;goronoaltaysky;goya;gräddost;graviera;gravyer;grevé;guayanés;gudbrandsdalsost;halloumi;havarti;hellim;herrgårdsost;herve;höfðingi;homoljski kozji;homoljski kravlji;homoljski;hoop;hushållsost;imsil;jameed;jāņi;jarlsberg;jibne baida;jibneh arabieh;kačkavalj;kadaka juust;kalari;kalathaki;kalimpong;karaván;karish;kaşar;kashkaval;kashkawan;kashta;kasseri;katiki;keçi peyniri;kefalograviera;kefalotyri;kelle peyniri;kenafa;kesong puti;khoya;kiladi;kirlihanım;kopanisti;korall;körözött;kostromskoy;kravská hrudka;krivovirski;küﬂü;küp;labne;labneh;ladotyri;lappi;latvijas;le wavreumont;leipäjuusto;liederkranz;lighvan;limburger;lingallin;liptauer;livno lor;lüneberg;mahali;maish krej;majdoule;malaka;manouri;manyas;mar de plata;maredsous;maribo;medynsky;menonita;metsovone;mihaliç;minas;mish;moale;mohant;molbo;mondseer;montafoner sauerkäse;monterey jack;moose;moskovsky;mozarella;muenster;myzithra;nabulsi;nacho cheese;nanoški;năsal;nguri;nøkkelost;norvegia;oaxaca;oázis;obruk;oka;olomoucké syrečky;oltermanni;omichka;orda;Örgü;otlu;pálpusztai;paneer;pannónia;panquehue;parenica;pasendale;paški sir;pepper jack;pichtogalo chanion;pikantny;pinconning;planinski;port wine;poshekhonsky;prästost;provel;pule;pultost;queijo canastra;queijo coalho;queijo cobocó;queijo de azeitão;queijo de colônia;queijo de nisa;queijo do pico;queijo do serro;queijo manteiga;queijo meia cura;queijo prato;queijo seco;queijoDoReino;quesillo;queso blanco;queso campesino;queso crema;queso crineja;queso cuajada;queso de cuajo;queso de mano;queso fresco;queso llanero;queso paipa;queso palmita;queso panela;queso parma de barinitas;queso pera;queso telita;questo costeño;queto;raejuusto;red hawk;reggianito;remoudou;renaico;requeijão;rice;rodoric;rossiysky;roumy;rubing;rushan;saga;sakura;salamura;saloio;samsø;santarém;são jorge;sardo;sayas;serra da estrela;sfela;shankish;shelal;sirene;sjeniĀki;Škripavac;smetankowyi;smoked;snøfrisk;sovetsky;soy;sremski;staazer;steirerkäse;string;sulguni;suluguni;surke;svecia;svrljiški belmuz;swiss;syr;syrian;talesh;tandil;tasty;teleme;telemea;telli;tilsiter;tiroler graukäse;tiromalama;tolminc;tounjski;trappista;travnički;tresse;tulum;tvaroh;tvorog;tybo;tyrolean grey;tyrozouli tzfat uglichsky;ukraїnskyi;urda;urdă;van;västerbottensost;vesterhavsost;vlašić;vurda;wagasi;xygalo;xynomizithra;xynotyro;yaroslavsky;zakusochny;zlatarski"
		  dim   strExpected() as string = strWords.split( ";" )
		  dim   strResults()  as string = wordListDT( "Cheeses" )
		  strExpected.sort
		  strResults.sort
		  Assert.areEqual( strExpected,strResults )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub boolean_wordlistDT__003_Test()
		  const strWords      as string = "affenpinscher;afghan hound;africanis;aidi;airedale terrier;akbash dog;akita;alano español;alapaha blue blood bulldog;alaskan klee kai;alaskan malamute;alaskan husky;alopekis;alpine dachsbracke;alsatian;american akita;american bulldog;american cocker spaniel;american english coonhound;american eskimo dog;american foxhound;american hairless terrier;american husky;american mastiff;american pit bull terrier;american staffordshire terrier;american staghound;american water spaniel;anatolian shepherd dog;anglo-francais de petite venerie;appenzeller sennenhund;arctic husky;argentine dogo;ariege pointer;ariegeois;artois hound;australian bulldog;australian cattle dog;australian jack russell terrier;australian kelpie;australian koolie;australian shepherd;australian silky terrier;australian stumpy tail cattle dog;australian terrier;austrian brandlbracke;austrian short-haired pinscher;azawakh;balkan hound;bandog;barbet;basenji;basset artésien normand;basset bleu de gascogne;basset fauve de bretagne;basset hound;bavarian mountain hound;beagle;beagle-harrier;bearded collie;bearded tibetan mastiff;beauceron;bedlington terrier;belgian griffon;belgian shepherd dog;bergamasco;berger blanc suisse;berger picard;berner sennenhund;bernese mountain dog;bichon frisé;biewer;billy;black mouth cur;black russian terrier;black and tan coonhound;bloodhound;blue heeler;blue paul terrier;blue picardy spaniel;bluetick coonhound;boerboel;bolognese;border collie;border terrier;borzoi;bosanski ostrodlaki gonic barak;bosnian tornjak;boston terrier;bouvier bernois;bouvier des ardennes;bouvier des flandres;boxer;boykin spaniel;bracco italiano;braque francais;braque saint-germain;braque d'auvergne;braque du bourbonnais;brazilian mastiff;brazilian terrier;briard;briquet griffon vendeen;brittany;broholmer;brussels griffon;bull terrier;bulldog;bullmastiff;bully kutta;ca de bou;cairn terrier;canaan dog;canadian eskimo dog;canadian inuit dog;cane corso;cao da serra de aires;caravan hound;cardigan welsh corgi;carpatin;catahoula bulldog;catahoula cur;catahoula hog dog;catahoula leopard dog;catalan sheepdog;caucasian ovcharka;cavalier king charles spaniel;central asia shepherd dog;cesky fousek;cesky terrier;chart polski;chesapeake bay retriever;chihuahua;chinese crested dog;chinook;chippiparai;chow chow;cirneco dell'etna;clumber spaniel;cockapoo;cocker spaniel;collie;cordoba fighting dog;corgi;coton de tulear;croatian sheepdog;curly coated retriever;czechoslovakian wolfdog;cão da serra de aires;cão de castro laboreiro;cão de fila da terceira;cão de fila de são miguel;cão de gado transmontano;dachshund;dakota sport retriever;dalmatian;dandie dinmont terrier;danish broholmer;danish farm dog;deerhound;deutsch drahthaar;deutsche bracke;deutscher wachtelhund;dhoki apso;do-khyi;doberman pinscher;dobermann;dogo cubano;dogue de bordeaux;dogue de majorque;drentse patrijshond;drever;dunker;dutch partridge dog;dutch shepherd dog;east siberian laika;english cocker spaniel;english coonhound;english foxhound;english mastiff;english pointer;english setter;english shepherd;english springer spaniel;english toy spaniel;english toy terrier;entlebucher mountain dog;epagneul picard;epagneul pont-audemer;eskimo dog;esquimaux;estonian hound;estrela mountain dog;eurasier;eurohound;feist;field spaniel;fila brasileiro;finnish hound;finnish lapphund;finnish spitz;flat-coated retriever;fox terrier;foxhound;francais blanc et noir;francais blanc et orange;francais tricolore;franzuskaya bolonka;french bulldog;french spaniel;french wirehaired pointing griffon;galgo espanol;galgo español;gawii;german longhaired pointer;german pinscher;german rough-haired pointer;german shepherd dog;german shorthaired pointer;german spaniel;german spitz;german wirehaired pointer;giant schnauzer;glen of imaal terrier;golden retriever;goldendoodle;gonczy polski;gordon setter;gos d'atura;gran mastin de borínquen;grand anglo-francais blanc et noir;grand anglo-francais blanc et orange;grand anglo-francais tricolore;grand basset griffon vendeen;grand bleu de gascogne;grand gascon saintongeois;grand griffon vendeen;great dane;great japanese dog;great pyrenees;greater swiss mountain dog;greek harehound;greek sheepdog;greek shepherd;greenland dog;greenland husky;greyhound;griffon bleu de gascogne;griffon bruxellois;griffon fauve de bretagne;griffon nivernais;groenendael;grosser schweizer sennenhund;gull dong;gull terr;hairless khala;haldenstovare;hamiltonstövare;hanover hound;harrier;havanese;himalayan sheepdog;hokkaido;hollandse herder;hovawart;hungarian greyhound;hungarian vizsla;hungarian wirehaired vizsla;huntaway;hygenhund;ibizan hound;icelandic sheepdog;indian bullterrier;indian mastiff;irish bull terrier;irish red and white setter;irish setter;irish staffordshire terrier;irish terrier;irish water spaniel;irish wolfhound;istarski kratkodlaki gonic;istarski ostrodlaki gonic;istrian coarse-haired hound;istrian sheepdog;italian greyhound;italian spinone;jdj american bulldog;jack russell terrier;jagdterrier;japanese chin;japanese spitz;japanese terrier;jindo;jonangi;jämthund;kai ken;kangal dog;kanni;karelian bear dog;kars dog;keeshond;kelb-tal fenek;kelpie;kerry blue terrier;king charles spaniel;kishu;kombai;komondor;kooikerhondje;koolie;korea jindo dog;korean mastiff;korthals griffon;krasky ovcar;kromfohrlander;kuvasz;kyi leo;labradoodle;labrador retriever;laekenois;lagotto romagnolo;lakeland terrier;lancashire heeler;landseer;lapinporokoira;large munsterlander;leonberger;lhasa apso;llewellyn setter;löwchen;mackenzie river husky;magyar agar;malinois;maltese;manchester terrier;maremma sheepdog;mastiff;meliteo kinidio;mexican hairless;mexican hairless dog;middle asian owtcharka;miniature australian shepherd;miniature bull terrier;miniature fox terrier;miniature pinscher;miniature schnauzer;mioritic;mixed-breed dog;moscovskaya storozhevaya sobaka;moscow watchdog;mountain burmese;mountain cur;mudhol hound;mudi;munsterlander;neapolitan mastiff;nebolish mastiff;newfoundland;norfolk terrier;norrbottenspets;northern inuit;norwegian buhund;norwegian elkhound;norwegian lundehund;norwich terrier;nova scotia duck-tolling retriever;old danish pointer;old english bulldog;old english sheepdog;olde englishe bulldogge;osterreichischer kurzhaariger pinscher;otterhound;otto;owczarek podhalanski;panja;papillon;parson russell terrier;patterdale terrier;pekeapoo;pekingese;pembroke welsh corgi;perdiguero de burgos;perro cimarron;perro de pastor mallorquin;perro de presa canario;perro de presa mallorquin;perro de toro;peruvian hairless dog;peruvian inca orchid;petit basset griffon vendeen;petit bleu de gascogne;petit brabancon;petit gascon saintongeois;phalène;pharaoh hound;phung san;picardy shepherd;picardy spaniel;pinscher;pit bull;plott hound;podenco canario;pointer;poitevin;polish greyhound;polish hound;polish lowland sheepdog;polish ogar;polish scenthound;polish sighthound;polish tatra sheepdog;polski owczarek nizinny;pomeranian;pont-audemer spaniel;poodle;porcelaine;portuguese podengo;portuguese pointer;portuguese shepherd dog;portuguese water dog;posavac hound;prazsky krysavik;pudelpointer;pug;puggle;puli;pumi;pyrenean mastiff;pyrenean mountain dog;pyrenean shepherd;queensland heeler;rafeiro do alentejo;rajapalayam;rampur greyhound;rat terrier;ratonero bodeguero andaluz;redbone coonhound;rhodesian ridgeback;romanian shepherd dog;rottweiler;rough collie;russell terrier;russian black terrier;russian hound;russian spaniel;russian toy terrier;russian tsvetnaya bolonka;russko-evropeiskaia laika;saarlooswolfhond;sabueso espanol;saluki;samoyed;sapsali;sarplaninac;schapendoes;schillerstovare;schipperke;schnauzer ;schnoodle;schweizer laufhund;schweizer niederlaufhund;scott's american bulldog;scottish deerhound;scottish terrier;sealyham terrier;segugio italiano;sennenhund/cattle dog;seppala siberian sleddog;serbian hound;serbian mountain hound;serbian tricolour hound;shar pei;shetland sheepdog;shiba inu;shih tzu;shikoku;shiloh shepherd dog;siberian husky;silken windhound;silky terrier;sindh mastiff;skye terrier;sloughi;slovak cuvac;slovakian hound;slovensky hrubosrsty stavac;smalandsstovare;small greek domestic dog;small munsterlander;smooth collie;smooth fox terrier;soft-coated wheaten terrier;south russian ovtcharka;southern white american bulldog;spanish alano;spanish galgo;spanish mastiff;spanish water dog;spinone italiano;spitz;springer spaniel;st. bernard;stabyhoun;staffordshire bull terrier;standard schnauzer;stephens cur;styrian coarse-haired hound;sussex spaniel;swedish elkhound;swedish farm dog;swedish lapphund;swedish vallhund;swiss shorthaired pinscher;tatra shepherd dog;teddy roosevelt terrier;tenterfield terrier;tervueren;tervuren;thai bangkaew dog;thai ridgeback;tibetan kyi apso;tibetan lhasa apso;tibetan mastiff;tibetan spaniel;tibetan terrier;tosa;toy bulldog;toy fox terrier;toy manchester terrier;toy mi-ki;transylvanian hound;treeing cur;treeing tennessee brindle;treeing walker coonhound;tsvetnaya bolonka;tyrolean hound;utonagan;valley bulldog;vizsla;volpino italiano;weimaraner;welsh corgi;welsh springer spaniel;welsh terrier;west highland white terrier;west siberian laika;westphalian dachsbracke;wetterhoun;whippet;white shepherd dog;wilkinson bulldog;wire fox terrier;wirehaired pointing griffon;xoloitzcuintle;yorkshire terrier;yugoslavian mountain hound;yugoslavian tricolour hound;bdutch shepherd dog;bdutch smoushondaffenpinscher;afghan hound;africanis;aidi;airedale terrier;akbash dog;akita;alano español;alapaha blue blood bulldog;alaskan klee kai;alaskan malamute;alaskan husky;alopekis;alpine dachsbracke;alsatian;american akita;american bulldog;american cocker spaniel;american english coonhound;american eskimo dog;american foxhound;american hairless terrier;american husky;american mastiff;american pit bull terrier;american staffordshire terrier;american staghound;american water spaniel;anatolian shepherd dog;anglo-francais de petite venerie;appenzeller sennenhund;arctic husky;argentine dogo;ariege pointer;ariegeois;artois hound;australian bulldog;australian cattle dog;australian jack russell terrier;australian kelpie;australian koolie;australian shepherd;australian silky terrier;australian stumpy tail cattle dog;australian terrier;austrian brandlbracke;austrian short-haired pinscher;azawakh;balkan hound;bandog;barbet;basenji;basset artésien normand;basset bleu de gascogne;basset fauve de bretagne;basset hound;bavarian mountain hound;beagle;beagle-harrier;bearded collie;bearded tibetan mastiff;beauceron;bedlington terrier;belgian griffon;belgian shepherd dog;bergamasco;berger blanc suisse;berger picard;berner sennenhund;bernese mountain dog;bichon frisé;biewer;billy;black mouth cur;black russian terrier;black and tan coonhound;bloodhound;blue heeler;blue paul terrier;blue picardy spaniel;bluetick coonhound;boerboel;bolognese;border collie;border terrier;borzoi;bosanski ostrodlaki gonic barak;bosnian tornjak;boston terrier;bouvier bernois;bouvier des ardennes;bouvier des flandres;boxer;boykin spaniel;bracco italiano;braque francais;braque saint-germain;braque d'auvergne;braque du bourbonnais;brazilian mastiff;brazilian terrier;briard;briquet griffon vendeen;brittany;broholmer;brussels griffon;bull terrier;bulldog;bullmastiff;bully kutta;ca de bou;cairn terrier;canaan dog;canadian eskimo dog;canadian inuit dog;cane corso;cao da serra de aires;caravan hound;cardigan welsh corgi;carpatin;catahoula bulldog;catahoula cur;catahoula hog dog;catahoula leopard dog;catalan sheepdog;caucasian ovcharka;cavalier king charles spaniel;central asia shepherd dog;cesky fousek;cesky terrier;chart polski;chesapeake bay retriever;chihuahua;chinese crested dog;chinook;chippiparai;chow chow;cirneco dell'etna;clumber spaniel;cockapoo;cocker spaniel;collie;cordoba fighting dog;corgi;coton de tulear;croatian sheepdog;curly coated retriever;czechoslovakian wolfdog;cão da serra de aires;cão de castro laboreiro;cão de fila da terceira;cão de fila de são miguel;cão de gado transmontano;dachshund;dakota sport retriever;dalmatian;dandie dinmont terrier;danish broholmer;danish farm dog;deerhound;deutsch drahthaar;deutsche bracke;deutscher wachtelhund;dhoki apso;do-khyi;doberman pinscher;dobermann;dogo cubano;dogue de bordeaux;dogue de majorque;drentse patrijshond;drever;dunker;dutch partridge dog;dutch shepherd dog;east siberian laika;english cocker spaniel;english coonhound;english foxhound;english mastiff;english pointer;english setter;english shepherd;english springer spaniel;english toy spaniel;english toy terrier;entlebucher mountain dog;epagneul picard;epagneul pont-audemer;eskimo dog;esquimaux;estonian hound;estrela mountain dog;eurasier;eurohound;feist;field spaniel;fila brasileiro;finnish hound;finnish lapphund;finnish spitz;flat-coated retriever;fox terrier;foxhound;francais blanc et noir;francais blanc et orange;francais tricolore;franzuskaya bolonka;french bulldog;french spaniel;french wirehaired pointing griffon;galgo espanol;galgo español;gawii;german longhaired pointer;german pinscher;german rough-haired pointer;german shepherd dog;german shorthaired pointer;german spaniel;german spitz;german wirehaired pointer;giant schnauzer;glen of imaal terrier;golden retriever;goldendoodle;gonczy polski;gordon setter;gos d'atura;gran mastin de borínquen;grand anglo-francais blanc et noir;grand anglo-francais blanc et orange;grand anglo-francais tricolore;grand basset griffon vendeen;grand bleu de gascogne;grand gascon saintongeois;grand griffon vendeen;great dane;great japanese dog;great pyrenees;greater swiss mountain dog;greek harehound;greek sheepdog;greek shepherd;greenland dog;greenland husky;greyhound;griffon bleu de gascogne;griffon bruxellois;griffon fauve de bretagne;griffon nivernais;groenendael;grosser schweizer sennenhund;gull dong;gull terr;hairless khala;haldenstovare;hamiltonstövare;hanover hound;harrier;havanese;himalayan sheepdog;hokkaido;hollandse herder;hovawart;hungarian greyhound;hungarian vizsla;hungarian wirehaired vizsla;huntaway;hygenhund;ibizan hound;icelandic sheepdog;indian bullterrier;indian mastiff;irish bull terrier;irish red and white setter;irish setter;irish staffordshire terrier;irish terrier;irish water spaniel;irish wolfhound;istarski kratkodlaki gonic;istarski ostrodlaki gonic;istrian coarse-haired hound;istrian sheepdog;italian greyhound;italian spinone;jdj american bulldog;jack russell terrier;jagdterrier;japanese chin;japanese spitz;japanese terrier;jindo;jonangi;jämthund;kai ken;kangal dog;kanni;karelian bear dog;kars dog;keeshond;kelb-tal fenek;kelpie;kerry blue terrier;king charles spaniel;kishu;kombai;komondor;kooikerhondje;koolie;korea jindo dog;korean mastiff;korthals griffon;krasky ovcar;kromfohrlander;kuvasz;kyi leo;labradoodle;labrador retriever;laekenois;lagotto romagnolo;lakeland terrier;lancashire heeler;landseer;lapinporokoira;large munsterlander;leonberger;lhasa apso;llewellyn setter;löwchen;mackenzie river husky;magyar agar;malinois;maltese;manchester terrier;maremma sheepdog;mastiff;meliteo kinidio;mexican hairless;mexican hairless dog;middle asian owtcharka;miniature australian shepherd;miniature bull terrier;miniature fox terrier;miniature pinscher;miniature schnauzer;mioritic;mixed-breed dog;moscovskaya storozhevaya sobaka;moscow watchdog;mountain burmese;mountain cur;mudhol hound;mudi;munsterlander;neapolitan mastiff;nebolish mastiff;newfoundland;norfolk terrier;norrbottenspets;northern inuit;norwegian buhund;norwegian elkhound;norwegian lundehund;norwich terrier;nova scotia duck-tolling retriever;old danish pointer;old english bulldog;old english sheepdog;olde englishe bulldogge;osterreichischer kurzhaariger pinscher;otterhound;otto;owczarek podhalanski;panja;papillon;parson russell terrier;patterdale terrier;pekeapoo;pekingese;pembroke welsh corgi;perdiguero de burgos;perro cimarron;perro de pastor mallorquin;perro de presa canario;perro de presa mallorquin;perro de toro;peruvian hairless dog;peruvian inca orchid;petit basset griffon vendeen;petit bleu de gascogne;petit brabancon;petit gascon saintongeois;phalène;pharaoh hound;phung san;picardy shepherd;picardy spaniel;pinscher;pit bull;plott hound;podenco canario;pointer;poitevin;polish greyhound;polish hound;polish lowland sheepdog;polish ogar;polish scenthound;polish sighthound;polish tatra sheepdog;polski owczarek nizinny;pomeranian;pont-audemer spaniel;poodle;porcelaine;portuguese podengo;portuguese pointer;portuguese shepherd dog;portuguese water dog;posavac hound;prazsky krysavik;pudelpointer;pug;puggle;puli;pumi;pyrenean mastiff;pyrenean mountain dog;pyrenean shepherd;queensland heeler;rafeiro do alentejo;rajapalayam;rampur greyhound;rat terrier;ratonero bodeguero andaluz;redbone coonhound;rhodesian ridgeback;romanian shepherd dog;rottweiler;rough collie;russell terrier;russian black terrier;russian hound;russian spaniel;russian toy terrier;russian tsvetnaya bolonka;russko-evropeiskaia laika;saarlooswolfhond;sabueso espanol;saluki;samoyed;sapsali;sarplaninac;schapendoes;schillerstovare;schipperke;schnauzer ;schnoodle;schweizer laufhund;schweizer niederlaufhund;scott's american bulldog;scottish deerhound;scottish terrier;sealyham terrier;segugio italiano;sennenhund/cattle dog;seppala siberian sleddog;serbian hound;serbian mountain hound;serbian tricolour hound;shar pei;shetland sheepdog;shiba inu;shih tzu;shikoku;shiloh shepherd dog;siberian husky;silken windhound;silky terrier;sindh mastiff;skye terrier;sloughi;slovak cuvac;slovakian hound;slovensky hrubosrsty stavac;smalandsstovare;small greek domestic dog;small munsterlander;smooth collie;smooth fox terrier;soft-coated wheaten terrier;south russian ovtcharka;southern white american bulldog;spanish alano;spanish galgo;spanish mastiff;spanish water dog;spinone italiano;spitz;springer spaniel;st. bernard;stabyhoun;staffordshire bull terrier;standard schnauzer;stephens cur;styrian coarse-haired hound;sussex spaniel;swedish elkhound;swedish farm dog;swedish lapphund;swedish vallhund;swiss shorthaired pinscher;tatra shepherd dog;teddy roosevelt terrier;tenterfield terrier;tervueren;tervuren;thai bangkaew dog;thai ridgeback;tibetan kyi apso;tibetan lhasa apso;tibetan mastiff;tibetan spaniel;tibetan terrier;tosa;toy bulldog;toy fox terrier;toy manchester terrier;toy mi-ki;transylvanian hound;treeing cur;treeing tennessee brindle;treeing walker coonhound;tsvetnaya bolonka;tyrolean hound;utonagan;valley bulldog;vizsla;volpino italiano;weimaraner;welsh corgi;welsh springer spaniel;welsh terrier;west highland white terrier;west siberian laika;westphalian dachsbracke;wetterhoun;whippet;white shepherd dog;wilkinson bulldog;wire fox terrier;wirehaired pointing griffon;xoloitzcuintle;yorkshire terrier;yugoslavian mountain hound;yugoslavian tricolour hound;bdutch shepherd dog;bdutch smoushond"
		  dim   strExpected() as string = strWords.split( ";" )
		  dim   strResults()  as string = wordListDT( "Dogs" )
		  strExpected.sort
		  strResults.sort
		  Assert.areEqual( strExpected,strResults )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub boolean_wordlistDT__004_Test()
		  const strWords      as string = "akee;apple;apricot;avocado;avocado;açaí;banana;bell pepper;bilberry;black sapote;blackberry;blackcurrant;blood orange;blueberry;boysenberry;buddha's hand;cantaloupe;cherimoya;cherry;chico fruit;chili pepper;clementine;cloudberry;coconut;corn kernel;crab apples;cranberry;cucumber;cucumber;currant;custard apple;damson;date;dragonfruit;durian;eggplant;elderberry;feijoa;fig;fingered citron;goji berry;gooseberry;grape;grapefruit;guava;honeyberry;honeydew;horned melon;huckleberry;jabuticaba;jackfruit;jambul;japanese plum;jostaberry;jujube;juniper berry;kiwano;kiwifruit;kumquat;lemon;lime;longan;loquat;lychee;mamin chino;mandarine;mango;mangosteen;marionberry;melon;miracle fruit;mulberry;nance;nectarine;olive;orange;papaya;passionfruit;pea;peach;pear;persimmon;pineapple;pineberry;pitaya;plantain;plum;plumcot;pluot;pomegranate;pomelo;prune;pumpkin;purple mangosteen;quince;raisin;rambutan ;raspberry;redcurrant;salak;salal berry;salmonberry;satsuma;soursop;squash;star apple;star fruit;strawberry;surinam cherry;tamarillo;tamarind;tangerine;tomato;ugli fruit;watermelon;white currant;white sapote;yuzu;zucchini"
		  dim   strExpected() as string = strWords.split( ";" )
		  dim   strResults()  as string = wordListDT( "Fruit" )
		  strExpected.sort
		  strResults.sort
		  Assert.areEqual( strExpected,strResults )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub boolean_wordlistDT__005_Test()
		  const strWords      as string = "a;ac;accumsan;ad;adipiscing;aenean;aliqua;aliquam;aliquet;aliquip;amet;anim;ante;aptent;arcu;at;auctor;augue;aute;bibendum;blandit;cillum;class;commodo;condimentum;congue;consectetur;consequat;conubia;convallis;cras;cubilia;culpa;cupidatat;curabitur;curae;cursus;dapibus;deserunt;diam;dictum;dictumst;dignissim;dis;do;dolor;dolore;donec;dui;duis;ea;efficitur;egestas;eget;eiusmod;eleifend;elementum;elit;enim;erat;eros;esse;est;et;etiam;eu;euismod;ex;excepteur;exercitation;facilisi;facilisis;fames;faucibus;felis;fermentum;feugiat;finibus;fringilla;fugiat;fusce;gravida;habitant;habitasse;hac;hendrerit;himenaeos;iaculis;id;imperdiet;in;inceptos;incididunt;integer;interdum;ipsum;irure;justo;labore;laboris;laborum;lacinia;lacus;laoreet;lectus;leo;libero;ligula;litora;lobortis;lorem;luctus;maecenas;magna;magnis;malesuada;massa;mattis;mauris;maximus;metus;mi;minim;molestie;mollis;mollit;montes;morbi;mus;nam;nascetur;natoque;nec;neque;netus;nibh;nisi;nisl;non;nostra;nostrud;nulla;nullam;nunc;occaecat;odio;officia;orci;ornare;pariatur;parturient;pellentesque;penatibus;per;pharetra;phasellus;placerat;platea;porta;porttitor;posuere;potenti;praesent;pretium;primis;proident;proin;pulvinar;purus;quam;qui;quis;quisque;reprehenderit;rhoncus;ridiculus;risus;rutrum;sagittis;sapien;scelerisque;sed;sem;semper;senectus;sint;sit;sociosqu;sodales;sollicitudin;sunt;suscipit;suspendisse;taciti;tellus;tempor;tempus;tincidunt;torquent;tortor;tristique;turpis;ullamco;ullamcorper;ultrices;ultricies;urna;ut;varius;vehicula;vel;velit;venenatis;veniam;vestibulum;vitae;vivamus;viverra;voluptate;volutpat;vulputate"
		  dim   strExpected() as string = strWords.split( ";" )
		  dim   strResults()  as string = wordListDT( "LoremIpsum" )
		  strExpected.sort
		  strResults.sort
		  Assert.areEqual( strExpected,strResults )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub boolean_wordlistDT__006_Test()
		  const strWords      as string = "ad;adipisicing;alcatra;aliqua;aliquip;amet;andouille;anim;aute;bacon;ball;beef;belly;biltong;boudin;bresaola;brisket;buffalo;burgdoggen;capicola;chicken;chop;chuck;cillum;consectetur;consequat;corned;cow;culpa;cupim;deserunt;dolor;dolore;doner;drumstick;duis;eiusmod;elit;enim;est;eu;excepteur;fatback;filet;flank;frankfurter;fugiat;ground;ham;hamburger;hock;id;in;incididunt;ipsum;irure;jalapeno;jerky;jowl;kevin;kielbasa;labore;laboris;laborum;landjaeger;leberkas;loin;lorem;magna;meatball;meatloaf;mignon;minim;mollit;nisi;non;nulla;occaecat;officia;pancetta;pariatur;pastrami;picanha;pig;porchetta;pork;proident;prosciutto;qui;quis;reprehenderit;ribeye;ribs;round;rump;salami;sausage;sed;shank;shankle;short;shoulder;sint;sirloin;spare;spicy;steak;strip;sunt;swine;tbone;tail;tempor;tenderloin;tip;tongue;tritip;turducken;turkey;ullamco;ut;veniam;venison;voluptate"
		  dim   strExpected() as string = strWords.split( ";" )
		  dim   strResults()  as string = wordListDT( "Meats" )
		  strExpected.sort
		  strResults.sort
		  Assert.areEqual( strExpected,strResults )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub boolean_wordlistDT__007_Test()
		  const strWords      as string = "abalone;alaska pollock;albacore tuna;american lobster;american shad;anchovy;herring;arctic char;atlantic mackerel;atlantic ocean perch;atlantic salmon;barracuda;barramundi;basa;bay scallop;black sea bass;black tiger shrimp;blue crab;blue mussel;blueﬁn tuna;bream;carp;catﬁsh;chilean sea bass;chinese white shrimp;chinook salmon;chum salmon;clam;cobia;cockle;cod;coho salmon;conch;crab;crayﬁsh;croaker;cusk;dogﬁsh;cuttleﬁsh;dory;drum;dungeness crab;eastern oyster;eel;blue marlin;escolar;european oyster;european sea bass;ﬂounder;freshwater shrimp;geoduck clam;greenshell mussel;grouper;gulf shrimp;haddock;hake;halibut;hardshell clam;hardshell clam;hoki;hybrid striped bass;jellyfish;jonah crab;king crab;kingklip;krill;lake victoria perch;lake whiteﬁsh;lingcod;moi;lobster;mahimahi;mako shark;monkﬁsh;mullet;octopus;opah;orange roughy;paciﬁc oyster;paciﬁc white shrimp;peekytoe crab;pink salmon;pink shrimp;pompano;dover sole;rainbow trout;rock shrimp;rockﬁsh;sableﬁsh;scup;sea scallop;sea urchin;shrimp;skate;smelt;snapper;snow crab;sockeye salmon;spanner crab;spiny lobster;squat lobster;squid;stone crab;sturgeon;surf clam;swordﬁsh;tilapia;tileﬁsh;turbot;wahoo;walleye;wolfﬁsh;yellow perch;yellowﬁn tuna;yellowtail"
		  dim   strExpected() as string = strWords.split( ";" )
		  dim   strResults()  as string = wordListDT( "Seafood" )
		  strExpected.sort
		  strResults.sort
		  Assert.areEqual( strExpected,strResults )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub boolean_wordlistDT__008_Test()
		  const strWords      as string = "amaranth leaves;artichoke;arugula;aubergine;avocado;baby corn;bamboo shoots;bean sprouts;beans;beet;beet greens;belgian endive;bell pepper;bibb lettuce;bitter gourd;bitter melon;bok choi;bok choy;boston lettuce;broccoli;brussels sprouts;burdock root;butterhead lettuce;calabash;candle corn;capers;carrot;cassava;cauliflower;celeriac;celery;celery root;celtuce;chayote;chinese broccoli;chinese spinach;collard greens;corn;cucumber;curly endive;curly kale;daikon radish;dandelion greens;edamame;eggplant;elephant garlic;english cucumber;escarole endive;fennel;fiddlehead;frisee endive;galangal;garlic;gherkin cucumber;ginger;gobo;grape leaves;green asparagus;green beans;green cabbage;green leaf lettuce;green onions;greens;hearts of palm;horseradish;iceberg lettuce;jerusalem artichoke;jícama;kai-lan;kale;kale;kohlrabi;kohlrabi greens;lacinato kale;leeks;lemongrass;lettuce;lotus root;lotus seed;maize;mushrooms;mustard greens;napa cabbage;nopales;okra;olive;onion;ornamental kale;pak choy;parsley;parsley root;parsnip;peas;peppers;pickling cucumbers;plantain;potato;pumpkin;purple asparagus;purslane;radicchio;radish;rapini;red cabbage;red leaf lettuce;romaine lettuce;rutabaga;savoy cabbage;scallions;shallots;snap beans;spinach;spinach;squash;string beans;sunchokes;sweet potato;swiss chard;swiss chard;taro;tomatillo;tomato;turnip;turnip greens;water chestnut;water spinach;watercress;wax beans;white asparagus;winter melon;yams;yuca;zucchini;green peas;snow peas;sugar snap peas"
		  dim   strExpected() as string = strWords.split( ";" )
		  dim   strResults()  as string = wordListDT( "Vegetables" )
		  strExpected.sort
		  strResults.sort
		  Assert.areEqual( strExpected,strResults )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub exception_paragraph_Bread__999_Test()
		  Assert.skipped( xojounit_NCM.kXojoUnit_Message_CantBeWritten )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub exception_paragraph_Cheeses__999_Test()
		  Assert.skipped( xojounit_NCM.kXojoUnit_Message_CantBeWritten )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub exception_paragraph_Dogs__999_Test()
		  Assert.skipped( xojounit_NCM.kXojoUnit_Message_CantBeWritten )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub exception_paragraph_Fruit__999_Test()
		  Assert.skipped( xojounit_NCM.kXojoUnit_Message_CantBeWritten )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub exception_paragraph_LoremIpsum__999_Test()
		  Assert.skipped( xojounit_NCM.kXojoUnit_Message_CantBeWritten )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub exception_paragraph_Meats__999_Test()
		  Assert.skipped( xojounit_NCM.kXojoUnit_Message_CantBeWritten )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub exception_paragraph_Seafood__999_Test()
		  Assert.skipped( xojounit_NCM.kXojoUnit_Message_CantBeWritten )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub exception_paragraph_Vegetables__999_Test()
		  Assert.skipped( xojounit_NCM.kXojoUnit_Message_CantBeWritten )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub exception_sentence_Bread__999_Test()
		  Assert.skipped( xojounit_NCM.kXojoUnit_Message_CantBeWritten )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub exception_sentence_Cheeses__999_Test()
		  Assert.skipped( xojounit_NCM.kXojoUnit_Message_CantBeWritten )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub exception_sentence_Dogs__999_Test()
		  Assert.skipped( xojounit_NCM.kXojoUnit_Message_CantBeWritten )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub exception_sentence_Fruit__999_Test()
		  Assert.skipped( xojounit_NCM.kXojoUnit_Message_CantBeWritten )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub exception_sentence_LoremIpsum__999_Test()
		  Assert.skipped( xojounit_NCM.kXojoUnit_Message_CantBeWritten )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub exception_sentence_Meats__999_Test()
		  Assert.skipped( xojounit_NCM.kXojoUnit_Message_CantBeWritten )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub exception_sentence_Seafood__999_Test()
		  Assert.skipped( xojounit_NCM.kXojoUnit_Message_CantBeWritten )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub exception_sentence_Vegetables__999_Test()
		  Assert.skipped( xojounit_NCM.kXojoUnit_Message_CantBeWritten )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub exception_word_Bread__999_Test()
		  Assert.skipped( xojounit_NCM.kXojoUnit_Message_CantBeWritten )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub exception_word_Cheeses__999_Test()
		  Assert.skipped( xojounit_NCM.kXojoUnit_Message_CantBeWritten )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub exception_word_Dogs__999_Test()
		  Assert.skipped( xojounit_NCM.kXojoUnit_Message_CantBeWritten )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub exception_word_Fruit__999_Test()
		  Assert.skipped( xojounit_NCM.kXojoUnit_Message_CantBeWritten )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub exception_word_LoremIpsum__999_Test()
		  Assert.skipped( xojounit_NCM.kXojoUnit_Message_CantBeWritten )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub exception_word_Meats__999_Test()
		  Assert.skipped( xojounit_NCM.kXojoUnit_Message_CantBeWritten )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub exception_word_Seafood__999_Test()
		  Assert.skipped( xojounit_NCM.kXojoUnit_Message_CantBeWritten )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub exception_word_Vegetables__999_Test()
		  Assert.skipped( xojounit_NCM.kXojoUnit_Message_CantBeWritten )
		End Sub
	#tag EndMethod


	#tag Note, Name = Test Numbering
		# Test Numbering
		
		all tests are listed as <module>_<method>__###_Test
		where as:
		<module> is the name of the module we are testing.  It can be shortned some.  like the "string_NCM" is reduced to "string"
		<method> is the name of the method we are testing.  If the method is overloaded, the name would be <method>_<return type>.
		
		### is a unique number.  first digit tells us what type of test.
		0 = uses the common constant that the whole test suite uses.
		1 = uses a unique constant (like email address or ip addresses) for that method, OR uses special tests.
		5 = are edge cases that we want to test.
		
		999 = the method can not be tested for some reason.
	#tag EndNote


	#tag Constant, Name = kNCM_UT_version, Type = String, Dynamic = False, Default = \"20190926", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Duration"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="FailedTestCount"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IncludeGroup"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IsRunning"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="NotImplementedCount"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="PassedTestCount"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RunTestCount"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SkippedTestCount"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="StopTestOnFail"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TestCount"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
