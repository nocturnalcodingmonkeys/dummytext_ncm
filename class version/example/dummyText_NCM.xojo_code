#tag Module
Protected Module dummyText_NCM
	#tag Method, Flags = &h0
		Function paragraph_Bread(firstParagraph as Boolean = false) As string
		  const strSpaces4 as string = "    "
		  const strSpaces3 as string = "   "
		  const strSpaces2 as string = "  "
		  
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstParagraph, sentence_Bread( firstParagraph ).Trim, "" )
		  dim r as new Random
		  dim int1 as integer = r.InRange( 1,3 )
		  dim int2 as integer = r.InRange( 0,3 )
		  dim int3 as integer = r.InRange( 0,3 )
		  
		  for each strSentence as string in sentence_Bread( int1+int2+int3 )
		    strRetVal.Append strSentence.trim
		  next
		  
		  return trim( join( strRetVal,strSpaces2 ) ).ReplaceAll( strSpaces4,strSpaces2 ).ReplaceAll( strSpaces3,strSpaces2 )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function paragraph_Bread(numberOfParagraphs as integer, firstParagraph as Boolean = false) As string()
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstParagraph, paragraph_Bread( true ), paragraph_Bread )
		  
		  for intCounter as integer = 2 to numberOfParagraphs
		    strRetVal.Append paragraph_Bread
		  next
		  
		  return strRetVal
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function paragraph_Cheeses(firstParagraph as Boolean = false) As string
		  const strSpaces4 as string = "    "
		  const strSpaces3 as string = "   "
		  const strSpaces2 as string = "  "
		  
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstParagraph, sentence_Cheeses( firstParagraph ).Trim, "" )
		  dim r as new Random
		  dim int1 as integer = r.InRange( 1,3 )
		  dim int2 as integer = r.InRange( 0,3 )
		  dim int3 as integer = r.InRange( 0,3 )
		  
		  for each strSentence as string in sentence_Cheeses( int1+int2+int3 )
		    strRetVal.Append strSentence.trim
		  next
		  
		  return trim( join( strRetVal,strSpaces2 ) ).ReplaceAll( strSpaces4,strSpaces2 ).ReplaceAll( strSpaces3,strSpaces2 )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function paragraph_Cheeses(numberOfParagraphs as integer, firstParagraph as Boolean = false) As string()
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstParagraph, paragraph_Cheeses( true ), paragraph_Cheeses )
		  
		  for intCounter as integer = 2 to numberOfParagraphs
		    strRetVal.Append paragraph_Cheeses
		  next
		  
		  return strRetVal
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function paragraph_Dogs(firstParagraph as Boolean = false) As string
		  const strSpaces4 as string = "    "
		  const strSpaces3 as string = "   "
		  const strSpaces2 as string = "  "
		  
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstParagraph, sentence_Dogs( firstParagraph ).Trim, "" )
		  dim r as new Random
		  dim int1 as integer = r.InRange( 1,3 )
		  dim int2 as integer = r.InRange( 0,3 )
		  dim int3 as integer = r.InRange( 0,3 )
		  
		  for each strSentence as string in sentence_Dogs( int1+int2+int3 )
		    strRetVal.Append strSentence.trim
		  next
		  
		  return trim( join( strRetVal,strSpaces2 ) ).ReplaceAll( strSpaces4,strSpaces2 ).ReplaceAll( strSpaces3,strSpaces2 )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function paragraph_Dogs(numberOfParagraphs as integer, firstParagraph as Boolean = false) As string()
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstParagraph, paragraph_Dogs( true ), paragraph_Dogs )
		  
		  for intCounter as integer = 2 to numberOfParagraphs
		    strRetVal.Append paragraph_Dogs
		  next
		  
		  return strRetVal
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function paragraph_Fruit(firstParagraph as Boolean = false) As string
		  const strSpaces4 as string = "    "
		  const strSpaces3 as string = "   "
		  const strSpaces2 as string = "  "
		  
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstParagraph, sentence_Fruit( firstParagraph ).Trim, "" )
		  dim r as new Random
		  dim int1 as integer = r.InRange( 1,3 )
		  dim int2 as integer = r.InRange( 0,3 )
		  dim int3 as integer = r.InRange( 0,3 )
		  
		  for each strSentence as string in sentence_Fruit( int1+int2+int3 )
		    strRetVal.Append strSentence.trim
		  next
		  
		  return trim( join( strRetVal,strSpaces2 ) ).ReplaceAll( strSpaces4,strSpaces2 ).ReplaceAll( strSpaces3,strSpaces2 )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function paragraph_Fruit(numberOfParagraphs as integer, firstParagraph as Boolean = false) As string()
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstParagraph, paragraph_Fruit( true ), paragraph_Fruit )
		  
		  for intCounter as integer = 2 to numberOfParagraphs
		    strRetVal.Append paragraph_Fruit
		  next
		  
		  return strRetVal
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function paragraph_LoremIpsum(firstParagraph as Boolean=false) As string
		  const strSpaces4 as string = "    "
		  const strSpaces3 as string = "   "
		  const strSpaces2 as string = "  "
		  
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstParagraph, sentence_LoremIpsum( firstParagraph ).Trim, "" )
		  dim r as new Random
		  dim int1 as integer = r.InRange( 1,3 )
		  dim int2 as integer = r.InRange( 0,3 )
		  dim int3 as integer = r.InRange( 0,3 )
		  
		  for each strSentence as string in sentence_LoremIpsum( int1+int2+int3 )
		    strRetVal.Append strSentence.trim
		  next
		  
		  return trim( join( strRetVal,strSpaces2 ) ).ReplaceAll( strSpaces4,strSpaces2 ).ReplaceAll( strSpaces3,strSpaces2 )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function paragraph_LoremIpsum(numberOfParagraphs as integer, firstParagraph as Boolean=false) As string()
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstParagraph, paragraph_LoremIpsum( true ), paragraph_LoremIpsum )
		  
		  for intCounter as integer = 2 to numberOfParagraphs
		    strRetVal.Append paragraph_LoremIpsum
		  next
		  
		  return strRetVal
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function paragraph_Meats(firstParagraph as Boolean = false) As string
		  const strSpaces4 as string = "    "
		  const strSpaces3 as string = "   "
		  const strSpaces2 as string = "  "
		  
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstParagraph, sentence_Meats( firstParagraph ).Trim, "" )
		  dim r as new Random
		  dim int1 as integer = r.InRange( 1,3 )
		  dim int2 as integer = r.InRange( 0,3 )
		  dim int3 as integer = r.InRange( 0,3 )
		  
		  for each strSentence as string in sentence_Meats( int1+int2+int3 )
		    strRetVal.Append strSentence.trim
		  next
		  
		  return trim( join( strRetVal,strSpaces2 ) ).ReplaceAll( strSpaces4,strSpaces2 ).ReplaceAll( strSpaces3,strSpaces2 )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function paragraph_Meats(numberOfParagraphs as integer, firstParagraph as Boolean = false) As string()
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstParagraph, paragraph_Meats( true ), paragraph_Meats )
		  
		  for intCounter as integer = 2 to numberOfParagraphs
		    strRetVal.Append paragraph_Meats
		  next
		  
		  return strRetVal
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function paragraph_Seafood(firstParagraph as Boolean = false) As string
		  const strSpaces4 as string = "    "
		  const strSpaces3 as string = "   "
		  const strSpaces2 as string = "  "
		  
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstParagraph, sentence_Seafood( firstParagraph ).Trim, "" )
		  dim r as new Random
		  dim int1 as integer = r.InRange( 1,3 )
		  dim int2 as integer = r.InRange( 0,3 )
		  dim int3 as integer = r.InRange( 0,3 )
		  
		  for each strSentence as string in sentence_Seafood( int1+int2+int3 )
		    strRetVal.Append strSentence.trim
		  next
		  
		  return trim( join( strRetVal,strSpaces2 ) ).ReplaceAll( strSpaces4,strSpaces2 ).ReplaceAll( strSpaces3,strSpaces2 )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function paragraph_Seafood(numberOfParagraphs as integer, firstParagraph as Boolean = false) As string()
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstParagraph, paragraph_Seafood( true ), paragraph_Seafood )
		  
		  for intCounter as integer = 2 to numberOfParagraphs
		    strRetVal.Append paragraph_Seafood
		  next
		  
		  return strRetVal
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function paragraph_Vegetables(firstParagraph as Boolean = false) As string
		  const strSpaces4 as string = "    "
		  const strSpaces3 as string = "   "
		  const strSpaces2 as string = "  "
		  
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstParagraph, sentence_Vegetables( firstParagraph ).Trim, "" )
		  dim r as new Random
		  dim int1 as integer = r.InRange( 1,3 )
		  dim int2 as integer = r.InRange( 0,3 )
		  dim int3 as integer = r.InRange( 0,3 )
		  
		  for each strSentence as string in sentence_Vegetables( int1+int2+int3 )
		    strRetVal.Append strSentence.trim
		  next
		  
		  return trim( join( strRetVal,strSpaces2 ) ).ReplaceAll( strSpaces4,strSpaces2 ).ReplaceAll( strSpaces3,strSpaces2 )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function paragraph_Vegetables(numberOfParagraphs as integer, firstParagraph as Boolean = false) As string()
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstParagraph, paragraph_Vegetables( true ), paragraph_Vegetables )
		  
		  for intCounter as integer = 2 to numberOfParagraphs
		    strRetVal.Append paragraph_Vegetables
		  next
		  
		  return strRetVal
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function sentence_Bread(firstSentence as Boolean = false) As string
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstSentence, kBreads_start+",", "" )
		  dim r as new Random
		  dim int1 as integer = r.InRange( 1,5 )
		  dim int2 as integer = r.InRange( 1,5 )
		  dim int3 as integer = r.InRange( 1,5 )
		  
		  for each strWord as string in word_Bread( int1+int2+int3 )
		    strRetVal.Append strWord
		  next
		  
		  return Trim( ( join( strRetVal, " " ) ) + "." ).CapitalizeNCM
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function sentence_Bread(numberOfSentences as integer, firstSentence as Boolean = false) As string()
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstSentence, sentence_Bread( firstSentence ), "" )
		  
		  for intCounter as integer = 1 to numberOfSentences
		    strRetVal.Append sentence_Bread
		  next
		  
		  return strRetVal
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function sentence_Cheeses(firstSentence as Boolean = false) As string
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstSentence, kCheeses_start+",", "" )
		  dim r as new Random
		  dim int1 as integer = r.InRange( 1,5 )
		  dim int2 as integer = r.InRange( 1,5 )
		  dim int3 as integer = r.InRange( 1,5 )
		  
		  for each strWord as string in word_Cheeses( int1+int2+int3 )
		    strRetVal.Append strWord
		  next
		  
		  return Trim( ( join( strRetVal, " " ) ) + "." ).CapitalizeNCM
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function sentence_Cheeses(numberOfSentences as integer, firstSentence as Boolean = false) As string()
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstSentence, sentence_Cheeses( firstSentence ), "" )
		  
		  for intCounter as integer = 1 to numberOfSentences
		    strRetVal.Append sentence_Cheeses
		  next
		  
		  return strRetVal
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function sentence_Dogs(firstSentence as Boolean = false) As string
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstSentence, kDogs_start+",", "" )
		  dim r as new Random
		  dim int1 as integer = r.InRange( 1,5 )
		  dim int2 as integer = r.InRange( 1,5 )
		  dim int3 as integer = r.InRange( 1,5 )
		  
		  for each strWord as string in word_Dogs( int1+int2+int3 )
		    strRetVal.Append strWord
		  next
		  
		  return Trim( ( join( strRetVal, " " ) ) + "." ).CapitalizeNCM
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function sentence_Dogs(numberOfSentences as integer, firstSentence as Boolean = false) As string()
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstSentence, sentence_Dogs( firstSentence ), "" )
		  
		  for intCounter as integer = 1 to numberOfSentences
		    strRetVal.Append sentence_Dogs
		  next
		  
		  return strRetVal
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function sentence_Fruit(firstSentence as Boolean = false) As string
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstSentence, kFruit_start+",", "" )
		  dim r as new Random
		  dim int1 as integer = r.InRange( 1,5 )
		  dim int2 as integer = r.InRange( 1,5 )
		  dim int3 as integer = r.InRange( 1,5 )
		  
		  for each strWord as string in word_Fruit( int1+int2+int3 )
		    strRetVal.Append strWord
		  next
		  
		  return Trim( ( join( strRetVal, " " ) ) + "." ).CapitalizeNCM
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function sentence_Fruit(numberOfSentences as integer, firstSentence as Boolean = false) As string()
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstSentence, sentence_Fruit( firstSentence ), "" )
		  
		  for intCounter as integer = 1 to numberOfSentences
		    strRetVal.Append sentence_Fruit
		  next
		  
		  return strRetVal
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function sentence_LoremIpsum(firstSentence as Boolean=false) As string
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstSentence, kLoremIpsum_start+",", "" )
		  dim r as new Random
		  dim int1 as integer = r.InRange( 1,5 )
		  dim int2 as integer = r.InRange( 1,5 )
		  dim int3 as integer = r.InRange( 1,5 )
		  
		  for each strWord as string in word_LoremIpsum( int1+int2+int3 )
		    strRetVal.Append strWord
		  next
		  
		  return Trim( ( join( strRetVal, " " ) ) + "." ).CapitalizeNCM
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function sentence_LoremIpsum(numberOfSentences as integer, firstSentence as Boolean=false) As string()
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstSentence, sentence_LoremIpsum( firstSentence ), "" )
		  
		  for intCounter as integer = 1 to numberOfSentences
		    strRetVal.Append sentence_LoremIpsum
		  next
		  
		  return strRetVal
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function sentence_Meats(firstSentence as Boolean = false) As string
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstSentence, kMeats_start+",", "" )
		  dim r as new Random
		  dim int1 as integer = r.InRange( 1,5 )
		  dim int2 as integer = r.InRange( 1,5 )
		  dim int3 as integer = r.InRange( 1,5 )
		  
		  for each strWord as string in word_Meats( int1+int2+int3 )
		    strRetVal.Append strWord
		  next
		  
		  return Trim( ( join( strRetVal, " " ) ) + "." ).CapitalizeNCM
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function sentence_Meats(numberOfSentences as integer, firstSentence as Boolean = false) As string()
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstSentence, sentence_Meats( firstSentence ), "" )
		  
		  for intCounter as integer = 1 to numberOfSentences
		    strRetVal.Append sentence_Meats
		  next
		  
		  return strRetVal
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function sentence_Seafood(firstSentence as Boolean = false) As string
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstSentence, kSeafood_start+",", "" )
		  dim r as new Random
		  dim int1 as integer = r.InRange( 1,5 )
		  dim int2 as integer = r.InRange( 1,5 )
		  dim int3 as integer = r.InRange( 1,5 )
		  
		  for each strWord as string in word_Seafood( int1+int2+int3 )
		    strRetVal.Append strWord
		  next
		  
		  return Trim( ( join( strRetVal, " " ) ) + "." ).CapitalizeNCM
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function sentence_Seafood(numberOfSentences as integer, firstSentence as Boolean = false) As string()
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstSentence, sentence_Seafood( firstSentence ), "" )
		  
		  for intCounter as integer = 1 to numberOfSentences
		    strRetVal.Append sentence_Seafood
		  next
		  
		  return strRetVal
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function sentence_Vegetables(firstSentence as Boolean = false) As string
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstSentence, kVegetables_start+",", "" )
		  dim r as new Random
		  dim int1 as integer = r.InRange( 1,5 )
		  dim int2 as integer = r.InRange( 1,5 )
		  dim int3 as integer = r.InRange( 1,5 )
		  
		  for each strWord as string in word_Vegetables( int1+int2+int3 )
		    strRetVal.Append strWord
		  next
		  
		  return Trim( ( join( strRetVal, " " ) ) + "." ).CapitalizeNCM
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function sentence_Vegetables(numberOfSentences as integer, firstSentence as Boolean = false) As string()
		  dim strRetVal() as string  
		  strRetVal.Append IF( firstSentence, sentence_Vegetables( firstSentence ), "" )
		  
		  for intCounter as integer = 1 to numberOfSentences
		    strRetVal.Append sentence_Vegetables
		  next
		  
		  return strRetVal
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function wordlistDT(type as string) As string()
		  select case type.lowercase
		  case "bread"
		    return kBreads_words.split( ";" )
		    
		  case "cheeses"
		    return kCheeses_words.split( ";" )
		    
		  case "dogs"
		    return kDogs_words.split( ";" )
		    
		  case "fruit"
		    return kFruit_words.split( ";" )
		    
		  case "loremipsum"
		    return kLoremIpsum_words.split( ";" )
		    
		  case "meats"
		    return kMeats_words.split( ";" )
		    
		  case "seafood"
		    return kSeafood_words.split( ";" )
		    
		  case "vegetables"
		    return kVegetables_words.split( ";" )
		  else
		    
		  end select
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function word_Bread() As string
		  dim strWords() as string = kBreads_words.split( ";" )
		  dim r as new Random
		  return strWords( r.InRange( 0,strWords.Ubound ) )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function word_Bread(numberOfWords as integer) As string()
		  dim strRetVal() as string
		  for intCounter as integer = 1 to numberOfWords
		    strRetVal.Append word_Bread
		  next
		  
		  return strRetVal
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function word_Cheeses() As string
		  dim strWords() as string = kCheeses_words.split( ";" )
		  dim r as new Random
		  return strWords( r.InRange( 0,strWords.Ubound ) )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function word_Cheeses(numberOfWords as integer) As string()
		  dim strRetVal() as string
		  for intCounter as integer = 1 to numberOfWords
		    strRetVal.Append word_Cheeses
		  next
		  
		  return strRetVal
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function word_Dogs() As string
		  dim strWords() as string = kDogs_words.split( ";" )
		  dim r as new Random
		  return strWords( r.InRange( 0,strWords.Ubound ) )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function word_Dogs(numberOfWords as integer) As string()
		  dim strRetVal() as string
		  for intCounter as integer = 1 to numberOfWords
		    strRetVal.Append word_Dogs
		  next
		  
		  return strRetVal
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function word_Fruit() As string
		  dim strWords() as string = kFruit_words.split( ";" )
		  dim r as new Random
		  return strWords( r.InRange( 0,strWords.Ubound ) )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function word_Fruit(numberOfWords as integer) As string()
		  dim strRetVal() as string
		  for intCounter as integer = 1 to numberOfWords
		    strRetVal.Append word_Fruit
		  next
		  
		  return strRetVal
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function word_LoremIpsum() As string
		  dim strWords() as string = kLoremIpsum_words.split( " " )
		  dim r as new Random
		  return strWords( r.InRange( 0,strWords.Ubound ) )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function word_LoremIpsum(numberOfWords as integer) As string()
		  dim strRetVal() as string
		  dim strWords() as string = kLoremIpsum_words.split( " " )
		  dim r as new Random
		  for intCounter as integer = 1 to numberOfWords
		    strRetVal.Append strWords( r.InRange( 0,strWords.Ubound ) )
		  next
		  
		  return strRetVal
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function word_Meats() As string
		  dim strWords() as string = kMeats_words.split( ";" )
		  dim r as new Random
		  return strWords( r.InRange( 0,strWords.Ubound ) )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function word_Meats(numberOfWords as integer) As string()
		  dim strRetVal() as string
		  for intCounter as integer = 1 to numberOfWords
		    strRetVal.Append word_Meats
		  next
		  
		  return strRetVal
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function word_Seafood() As string
		  dim strWords() as string = kSeafood_words.split( ";" )
		  dim r as new Random
		  return strWords( r.InRange( 0,strWords.Ubound ) )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function word_Seafood(numberOfWords as integer) As string()
		  dim strRetVal() as string
		  for intCounter as integer = 1 to numberOfWords
		    strRetVal.Append word_Seafood
		  next
		  
		  return strRetVal
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function word_Vegetables() As string
		  dim strWords() as string = kVegetables_words.split( ";" )
		  dim r as new Random
		  return strWords( r.InRange( 0,strWords.Ubound ) )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function word_Vegetables(numberOfWords as integer) As string()
		  dim strRetVal() as string
		  for intCounter as integer = 1 to numberOfWords
		    strRetVal.Append word_Vegetables
		  next
		  
		  return strRetVal
		End Function
	#tag EndMethod


	#tag Constant, Name = kBreads_start, Type = String, Dynamic = False, Default = \"Bread ipsum dolor sit amet", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kBreads_words, Type = String, Dynamic = False, Default = \"aish merahrah;ajdov kruh;anadama bread;anpan;appam;arepa;baba;bagel;baguette;bakrakhani;bakshalu;balep korkun;bammy;banana bread;bannok;bara birth;barbari bread;barm cake;barmbrack;barotta;bastone;baziama;bazin;beer bread;belgian waffle;bhakri;biaa;bialy;bing;biscuit;black bread;bobbatlu;bolani;bolo do caco;borlengo;borodinsky;boule;brambor\xC3\xA1\xC4\x8Dek bread roll;breadstick;brioche;broa;brown bread;bublik;buckwheat;bun;canadian white;carrot bread;\xC4\x8Cesnica;challah;chapati;chickpea bread;cholerm\xC3\xBCs;christmas wafer;ciabatta;coppia ferrarese;cornbread;cottage load;cracker;cr\xC3\xAApe;crisp bread;crispy bread;croutons;crumpet;cuban bread;curry bread;damper;dampfnudel;dorayaki;dosa;dry bread;eggette;english muffin;fari;filone;flatbread;flatbr\xC3\xB8d;flatkaka;focaccia;fruit bread;green onion pancake;hallulla;hard dough bread;hardebrood;hardtack;himbasha;hoecake;injera;johnnycake;ka\'ak;kalach kaya toast;khakhra;khanom bueang;khubz;ki\xEF\xAC\x82i;kisra;kulcha;laffa;lagana;lahoh;laobing;lavash;leavened;lefse;malooga;mantou;markook;marraqueta;massa sovada;matzo;melonpan;miche;michetta;mollete;montrealStyle bagel;multigrain bread;naan;ngome;obbatu;obwarzanek krakowski;pain d\'epi;pain de mie;pan dulce;pana carasau;pana ticinese;panbrioche;pancake;pancarr\xC3\xA9;pandesal;pandoro;pane di altamura;panettone;panfocaccia;papad;papadum;paratha;parotta;paximathia;penia;pex bread;piadina;pikelet;pistolet;pita;pizza;poga\xC4\x8Da;potato bread;potato pancake;potbrood;pretzel;proja;pumpernickel;puran poli;qistibi;quick bread;rewena bread;rice bread;roti;rugbr\xC3\xB8d;rumali roti;ryainjun;rye bread;rye;sacramental bread;saj bread;saltRising bread;sangak;scone;sgabeo;shirmal;shoti;soboro bread;soda bread;sourdough bread;sourdough;speckendick;spelt bread;sprouted bread;sweet bread;sweet bun;taboon bread;taftan;tandoor bread;teacake;texas toast;tiger bread;toasted bread;tortilla;tsoureki;ttongppang;tunnbr\xC3\xB6d;v\xC3\xA1no\xC4\x8Dka;vienna bread;waffle;wheat;white bread;whole wheat bread;yeast;yufka;zopf;zwieback", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kCheeses_start, Type = String, Dynamic = False, Default = \"Cheese ipsum dolor sit amet", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kCheeses_words, Type = String, Dynamic = False, Default = \"abaza;abertam;adadero;\xC3\x84delost;adobera;adygheysky;akkawi;altaysky;anari;a\xC3\xB1ejo;antep;anthotyros;areesh;armola;arnavut;atleet;aura;ayibe;bachensteiner;baladi;bal\xC4\xB1kesir;bandel;basket;belo sirenje;bergenost bergk\xC3\xA4se;beyaz peynir;bilozhar;bl\xC3\xA5 gotland;bohinc jo\xC5\xBEe;br\xC3\xA2nz\xC7\x8E de burduf;br\xC3\xA2nz\xC4\x83 de suhaia;br\xC3\xA2nz\xC4\x83 de vaci;brick;brimsen brunost;brussels;bryndza;bukovinskyi;byaslag;\xC3\x87amur;caravane;ca\xC5\x9F;ca\xC5\x9Fcaval;castelo branco;catupiry;\xC3\x87erkez f\xC3\xBCme;chanco;chaque\xC3\xB1o;charkassiye;chechil;cheddar;cheese curds;cherni vit;chhena;chhurpi;chiapas;chihuahua;chimay cheeses;chloro;chubut;chura kampo;chura loenpa;chyorny altai;circassian;civil;\xC3\x87\xC3\xB6kelek;colby;colbyJack;colorado blackie;\xC3\x87\xC3\xB6mlek;cotija;cottage cream cheese;crema;cremoso;creole cream cheese;criollo;cuajada;cup;dahi chhena;danbo;dangke;danilovsky;danish blue;dil;dimsi;dobrodar;domiati;dorogobuzhsky;dorozhny;dziugas;edirne;eesti juust;enredo;esrom;ezine;farmer farmer;feta;flower of rajya;formaela;fynbo;galotyri;gamalost;gauda;gbenja;geilost;gelundener k\xC3\xA4se;golandsky;goronoaltaysky;goya;gr\xC3\xA4ddost;graviera;gravyer;grev\xC3\xA9;guayan\xC3\xA9s;gudbrandsdalsost;halloumi;havarti;hellim;herrg\xC3\xA5rdsost;herve;h\xC3\xB6f\xC3\xB0ingi;homoljski kozji;homoljski kravlji;homoljski;hoop;hush\xC3\xA5llsost;imsil;jameed;j\xC4\x81\xC5\x86i;jarlsberg;jibne baida;jibneh arabieh;ka\xC4\x8Dkavalj;kadaka juust;kalari;kalathaki;kalimpong;karav\xC3\xA1n;karish;ka\xC5\x9Far;kashkaval;kashkawan;kashta;kasseri;katiki;ke\xC3\xA7i peyniri;kefalograviera;kefalotyri;kelle peyniri;kenafa;kesong puti;khoya;kiladi;kirlihan\xC4\xB1m;kopanisti;korall;k\xC3\xB6r\xC3\xB6z\xC3\xB6tt;kostromskoy;kravsk\xC3\xA1 hrudka;krivovirski;k\xC3\xBC\xEF\xAC\x82\xC3\xBC;k\xC3\xBCp;labne;labneh;ladotyri;lappi;latvijas;le wavreumont;leip\xC3\xA4juusto;liederkranz;lighvan;limburger;lingallin;liptauer;livno lor;l\xC3\xBCneberg;mahali;maish krej;majdoule;malaka;manouri;manyas;mar de plata;maredsous;maribo;medynsky;menonita;metsovone;mihali\xC3\xA7;minas;mish;moale;mohant;molbo;mondseer;montafoner sauerk\xC3\xA4se;monterey jack;moose;moskovsky;mozarella;muenster;myzithra;nabulsi;nacho cheese;nano\xC5\xA1ki;n\xC4\x83sal;nguri;n\xC3\xB8kkelost;norvegia;oaxaca;o\xC3\xA1zis;obruk;oka;olomouck\xC3\xA9 syre\xC4\x8Dky;oltermanni;omichka;orda;\xC3\x96rg\xC3\xBC;otlu;p\xC3\xA1lpusztai;paneer;pann\xC3\xB3nia;panquehue;parenica;pasendale;pa\xC5\xA1ki sir;pepper jack;pichtogalo chanion;pikantny;pinconning;planinski;port wine;poshekhonsky;pr\xC3\xA4stost;provel;pule;pultost;queijo canastra;queijo coalho;queijo coboc\xC3\xB3;queijo de azeit\xC3\xA3o;queijo de col\xC3\xB4nia;queijo de nisa;queijo do pico;queijo do serro;queijo manteiga;queijo meia cura;queijo prato;queijo seco;queijoDoReino;quesillo;queso blanco;queso campesino;queso crema;queso crineja;queso cuajada;queso de cuajo;queso de mano;queso fresco;queso llanero;queso paipa;queso palmita;queso panela;queso parma de barinitas;queso pera;queso telita;questo coste\xC3\xB1o;queto;raejuusto;red hawk;reggianito;remoudou;renaico;requeij\xC3\xA3o;rice;rodoric;rossiysky;roumy;rubing;rushan;saga;sakura;salamura;saloio;sams\xC3\xB8;santar\xC3\xA9m;s\xC3\xA3o jorge;sardo;sayas;serra da estrela;sfela;shankish;shelal;sirene;sjeni\xC4\x80ki;\xC5\xA0kripavac;smetankowyi;smoked;sn\xC3\xB8frisk;sovetsky;soy;sremski;staazer;steirerk\xC3\xA4se;string;sulguni;suluguni;surke;svecia;svrlji\xC5\xA1ki belmuz;swiss;syr;syrian;talesh;tandil;tasty;teleme;telemea;telli;tilsiter;tiroler grauk\xC3\xA4se;tiromalama;tolminc;tounjski;trappista;travni\xC4\x8Dki;tresse;tulum;tvaroh;tvorog;tybo;tyrolean grey;tyrozouli tzfat uglichsky;ukra\xD1\x97nskyi;urda;urd\xC4\x83;van;v\xC3\xA4sterbottensost;vesterhavsost;vla\xC5\xA1i\xC4\x87;vurda;wagasi;xygalo;xynomizithra;xynotyro;yaroslavsky;zakusochny;zlatarski", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kDogs_start, Type = String, Dynamic = False, Default = \"Dogs ipsum dolor sit amet", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kDogs_words, Type = String, Dynamic = False, Default = \"affenpinscher;afghan hound;africanis;aidi;airedale terrier;akbash dog;akita;alano espa\xC3\xB1ol;alapaha blue blood bulldog;alaskan klee kai;alaskan malamute;alaskan husky;alopekis;alpine dachsbracke;alsatian;american akita;american bulldog;american cocker spaniel;american english coonhound;american eskimo dog;american foxhound;american hairless terrier;american husky;american mastiff;american pit bull terrier;american staffordshire terrier;american staghound;american water spaniel;anatolian shepherd dog;anglo-francais de petite venerie;appenzeller sennenhund;arctic husky;argentine dogo;ariege pointer;ariegeois;artois hound;australian bulldog;australian cattle dog;australian jack russell terrier;australian kelpie;australian koolie;australian shepherd;australian silky terrier;australian stumpy tail cattle dog;australian terrier;austrian brandlbracke;austrian short-haired pinscher;azawakh;balkan hound;bandog;barbet;basenji;basset art\xC3\xA9sien normand;basset bleu de gascogne;basset fauve de bretagne;basset hound;bavarian mountain hound;beagle;beagle-harrier;bearded collie;bearded tibetan mastiff;beauceron;bedlington terrier;belgian griffon;belgian shepherd dog;bergamasco;berger blanc suisse;berger picard;berner sennenhund;bernese mountain dog;bichon fris\xC3\xA9;biewer;billy;black mouth cur;black russian terrier;black and tan coonhound;bloodhound;blue heeler;blue paul terrier;blue picardy spaniel;bluetick coonhound;boerboel;bolognese;border collie;border terrier;borzoi;bosanski ostrodlaki gonic barak;bosnian tornjak;boston terrier;bouvier bernois;bouvier des ardennes;bouvier des flandres;boxer;boykin spaniel;bracco italiano;braque francais;braque saint-germain;braque d\'auvergne;braque du bourbonnais;brazilian mastiff;brazilian terrier;briard;briquet griffon vendeen;brittany;broholmer;brussels griffon;bull terrier;bulldog;bullmastiff;bully kutta;ca de bou;cairn terrier;canaan dog;canadian eskimo dog;canadian inuit dog;cane corso;cao da serra de aires;caravan hound;cardigan welsh corgi;carpatin;catahoula bulldog;catahoula cur;catahoula hog dog;catahoula leopard dog;catalan sheepdog;caucasian ovcharka;cavalier king charles spaniel;central asia shepherd dog;cesky fousek;cesky terrier;chart polski;chesapeake bay retriever;chihuahua;chinese crested dog;chinook;chippiparai;chow chow;cirneco dell\'etna;clumber spaniel;cockapoo;cocker spaniel;collie;cordoba fighting dog;corgi;coton de tulear;croatian sheepdog;curly coated retriever;czechoslovakian wolfdog;c\xC3\xA3o da serra de aires;c\xC3\xA3o de castro laboreiro;c\xC3\xA3o de fila da terceira;c\xC3\xA3o de fila de s\xC3\xA3o miguel;c\xC3\xA3o de gado transmontano;dachshund;dakota sport retriever;dalmatian;dandie dinmont terrier;danish broholmer;danish farm dog;deerhound;deutsch drahthaar;deutsche bracke;deutscher wachtelhund;dhoki apso;do-khyi;doberman pinscher;dobermann;dogo cubano;dogue de bordeaux;dogue de majorque;drentse patrijshond;drever;dunker;dutch partridge dog;dutch shepherd dog;east siberian laika;english cocker spaniel;english coonhound;english foxhound;english mastiff;english pointer;english setter;english shepherd;english springer spaniel;english toy spaniel;english toy terrier;entlebucher mountain dog;epagneul picard;epagneul pont-audemer;eskimo dog;esquimaux;estonian hound;estrela mountain dog;eurasier;eurohound;feist;field spaniel;fila brasileiro;finnish hound;finnish lapphund;finnish spitz;flat-coated retriever;fox terrier;foxhound;francais blanc et noir;francais blanc et orange;francais tricolore;franzuskaya bolonka;french bulldog;french spaniel;french wirehaired pointing griffon;galgo espanol;galgo espa\xC3\xB1ol;gawii;german longhaired pointer;german pinscher;german rough-haired pointer;german shepherd dog;german shorthaired pointer;german spaniel;german spitz;german wirehaired pointer;giant schnauzer;glen of imaal terrier;golden retriever;goldendoodle;gonczy polski;gordon setter;gos d\'atura;gran mastin de bor\xC3\xADnquen;grand anglo-francais blanc et noir;grand anglo-francais blanc et orange;grand anglo-francais tricolore;grand basset griffon vendeen;grand bleu de gascogne;grand gascon saintongeois;grand griffon vendeen;great dane;great japanese dog;great pyrenees;greater swiss mountain dog;greek harehound;greek sheepdog;greek shepherd;greenland dog;greenland husky;greyhound;griffon bleu de gascogne;griffon bruxellois;griffon fauve de bretagne;griffon nivernais;groenendael;grosser schweizer sennenhund;gull dong;gull terr;hairless khala;haldenstovare;hamiltonst\xC3\xB6vare;hanover hound;harrier;havanese;himalayan sheepdog;hokkaido;hollandse herder;hovawart;hungarian greyhound;hungarian vizsla;hungarian wirehaired vizsla;huntaway;hygenhund;ibizan hound;icelandic sheepdog;indian bullterrier;indian mastiff;irish bull terrier;irish red and white setter;irish setter;irish staffordshire terrier;irish terrier;irish water spaniel;irish wolfhound;istarski kratkodlaki gonic;istarski ostrodlaki gonic;istrian coarse-haired hound;istrian sheepdog;italian greyhound;italian spinone;jdj american bulldog;jack russell terrier;jagdterrier;japanese chin;japanese spitz;japanese terrier;jindo;jonangi;j\xC3\xA4mthund;kai ken;kangal dog;kanni;karelian bear dog;kars dog;keeshond;kelb-tal fenek;kelpie;kerry blue terrier;king charles spaniel;kishu;kombai;komondor;kooikerhondje;koolie;korea jindo dog;korean mastiff;korthals griffon;krasky ovcar;kromfohrlander;kuvasz;kyi leo;labradoodle;labrador retriever;laekenois;lagotto romagnolo;lakeland terrier;lancashire heeler;landseer;lapinporokoira;large munsterlander;leonberger;lhasa apso;llewellyn setter;l\xC3\xB6wchen;mackenzie river husky;magyar agar;malinois;maltese;manchester terrier;maremma sheepdog;mastiff;meliteo kinidio;mexican hairless;mexican hairless dog;middle asian owtcharka;miniature australian shepherd;miniature bull terrier;miniature fox terrier;miniature pinscher;miniature schnauzer;mioritic;mixed-breed dog;moscovskaya storozhevaya sobaka;moscow watchdog;mountain burmese;mountain cur;mudhol hound;mudi;munsterlander;neapolitan mastiff;nebolish mastiff;newfoundland;norfolk terrier;norrbottenspets;northern inuit;norwegian buhund;norwegian elkhound;norwegian lundehund;norwich terrier;nova scotia duck-tolling retriever;old danish pointer;old english bulldog;old english sheepdog;olde englishe bulldogge;osterreichischer kurzhaariger pinscher;otterhound;otto;owczarek podhalanski;panja;papillon;parson russell terrier;patterdale terrier;pekeapoo;pekingese;pembroke welsh corgi;perdiguero de burgos;perro cimarron;perro de pastor mallorquin;perro de presa canario;perro de presa mallorquin;perro de toro;peruvian hairless dog;peruvian inca orchid;petit basset griffon vendeen;petit bleu de gascogne;petit brabancon;petit gascon saintongeois;phal\xC3\xA8ne;pharaoh hound;phung san;picardy shepherd;picardy spaniel;pinscher;pit bull;plott hound;podenco canario;pointer;poitevin;polish greyhound;polish hound;polish lowland sheepdog;polish ogar;polish scenthound;polish sighthound;polish tatra sheepdog;polski owczarek nizinny;pomeranian;pont-audemer spaniel;poodle;porcelaine;portuguese podengo;portuguese pointer;portuguese shepherd dog;portuguese water dog;posavac hound;prazsky krysavik;pudelpointer;pug;puggle;puli;pumi;pyrenean mastiff;pyrenean mountain dog;pyrenean shepherd;queensland heeler;rafeiro do alentejo;rajapalayam;rampur greyhound;rat terrier;ratonero bodeguero andaluz;redbone coonhound;rhodesian ridgeback;romanian shepherd dog;rottweiler;rough collie;russell terrier;russian black terrier;russian hound;russian spaniel;russian toy terrier;russian tsvetnaya bolonka;russko-evropeiskaia laika;saarlooswolfhond;sabueso espanol;saluki;samoyed;sapsali;sarplaninac;schapendoes;schillerstovare;schipperke;schnauzer ;schnoodle;schweizer laufhund;schweizer niederlaufhund;scott\'s american bulldog;scottish deerhound;scottish terrier;sealyham terrier;segugio italiano;sennenhund/cattle dog;seppala siberian sleddog;serbian hound;serbian mountain hound;serbian tricolour hound;shar pei;shetland sheepdog;shiba inu;shih tzu;shikoku;shiloh shepherd dog;siberian husky;silken windhound;silky terrier;sindh mastiff;skye terrier;sloughi;slovak cuvac;slovakian hound;slovensky hrubosrsty stavac;smalandsstovare;small greek domestic dog;small munsterlander;smooth collie;smooth fox terrier;soft-coated wheaten terrier;south russian ovtcharka;southern white american bulldog;spanish alano;spanish galgo;spanish mastiff;spanish water dog;spinone italiano;spitz;springer spaniel;st. bernard;stabyhoun;staffordshire bull terrier;standard schnauzer;stephens cur;styrian coarse-haired hound;sussex spaniel;swedish elkhound;swedish farm dog;swedish lapphund;swedish vallhund;swiss shorthaired pinscher;tatra shepherd dog;teddy roosevelt terrier;tenterfield terrier;tervueren;tervuren;thai bangkaew dog;thai ridgeback;tibetan kyi apso;tibetan lhasa apso;tibetan mastiff;tibetan spaniel;tibetan terrier;tosa;toy bulldog;toy fox terrier;toy manchester terrier;toy mi-ki;transylvanian hound;treeing cur;treeing tennessee brindle;treeing walker coonhound;tsvetnaya bolonka;tyrolean hound;utonagan;valley bulldog;vizsla;volpino italiano;weimaraner;welsh corgi;welsh springer spaniel;welsh terrier;west highland white terrier;west siberian laika;westphalian dachsbracke;wetterhoun;whippet;white shepherd dog;wilkinson bulldog;wire fox terrier;wirehaired pointing griffon;xoloitzcuintle;yorkshire terrier;yugoslavian mountain hound;yugoslavian tricolour hound;bdutch shepherd dog;bdutch smoushondaffenpinscher;afghan hound;africanis;aidi;airedale terrier;akbash dog;akita;alano espa\xC3\xB1ol;alapaha blue blood bulldog;alaskan klee kai;alaskan malamute;alaskan husky;alopekis;alpine dachsbracke;alsatian;american akita;american bulldog;american cocker spaniel;american english coonhound;american eskimo dog;american foxhound;american hairless terrier;american husky;american mastiff;american pit bull terrier;american staffordshire terrier;american staghound;american water spaniel;anatolian shepherd dog;anglo-francais de petite venerie;appenzeller sennenhund;arctic husky;argentine dogo;ariege pointer;ariegeois;artois hound;australian bulldog;australian cattle dog;australian jack russell terrier;australian kelpie;australian koolie;australian shepherd;australian silky terrier;australian stumpy tail cattle dog;australian terrier;austrian brandlbracke;austrian short-haired pinscher;azawakh;balkan hound;bandog;barbet;basenji;basset art\xC3\xA9sien normand;basset bleu de gascogne;basset fauve de bretagne;basset hound;bavarian mountain hound;beagle;beagle-harrier;bearded collie;bearded tibetan mastiff;beauceron;bedlington terrier;belgian griffon;belgian shepherd dog;bergamasco;berger blanc suisse;berger picard;berner sennenhund;bernese mountain dog;bichon fris\xC3\xA9;biewer;billy;black mouth cur;black russian terrier;black and tan coonhound;bloodhound;blue heeler;blue paul terrier;blue picardy spaniel;bluetick coonhound;boerboel;bolognese;border collie;border terrier;borzoi;bosanski ostrodlaki gonic barak;bosnian tornjak;boston terrier;bouvier bernois;bouvier des ardennes;bouvier des flandres;boxer;boykin spaniel;bracco italiano;braque francais;braque saint-germain;braque d\'auvergne;braque du bourbonnais;brazilian mastiff;brazilian terrier;briard;briquet griffon vendeen;brittany;broholmer;brussels griffon;bull terrier;bulldog;bullmastiff;bully kutta;ca de bou;cairn terrier;canaan dog;canadian eskimo dog;canadian inuit dog;cane corso;cao da serra de aires;caravan hound;cardigan welsh corgi;carpatin;catahoula bulldog;catahoula cur;catahoula hog dog;catahoula leopard dog;catalan sheepdog;caucasian ovcharka;cavalier king charles spaniel;central asia shepherd dog;cesky fousek;cesky terrier;chart polski;chesapeake bay retriever;chihuahua;chinese crested dog;chinook;chippiparai;chow chow;cirneco dell\'etna;clumber spaniel;cockapoo;cocker spaniel;collie;cordoba fighting dog;corgi;coton de tulear;croatian sheepdog;curly coated retriever;czechoslovakian wolfdog;c\xC3\xA3o da serra de aires;c\xC3\xA3o de castro laboreiro;c\xC3\xA3o de fila da terceira;c\xC3\xA3o de fila de s\xC3\xA3o miguel;c\xC3\xA3o de gado transmontano;dachshund;dakota sport retriever;dalmatian;dandie dinmont terrier;danish broholmer;danish farm dog;deerhound;deutsch drahthaar;deutsche bracke;deutscher wachtelhund;dhoki apso;do-khyi;doberman pinscher;dobermann;dogo cubano;dogue de bordeaux;dogue de majorque;drentse patrijshond;drever;dunker;dutch partridge dog;dutch shepherd dog;east siberian laika;english cocker spaniel;english coonhound;english foxhound;english mastiff;english pointer;english setter;english shepherd;english springer spaniel;english toy spaniel;english toy terrier;entlebucher mountain dog;epagneul picard;epagneul pont-audemer;eskimo dog;esquimaux;estonian hound;estrela mountain dog;eurasier;eurohound;feist;field spaniel;fila brasileiro;finnish hound;finnish lapphund;finnish spitz;flat-coated retriever;fox terrier;foxhound;francais blanc et noir;francais blanc et orange;francais tricolore;franzuskaya bolonka;french bulldog;french spaniel;french wirehaired pointing griffon;galgo espanol;galgo espa\xC3\xB1ol;gawii;german longhaired pointer;german pinscher;german rough-haired pointer;german shepherd dog;german shorthaired pointer;german spaniel;german spitz;german wirehaired pointer;giant schnauzer;glen of imaal terrier;golden retriever;goldendoodle;gonczy polski;gordon setter;gos d\'atura;gran mastin de bor\xC3\xADnquen;grand anglo-francais blanc et noir;grand anglo-francais blanc et orange;grand anglo-francais tricolore;grand basset griffon vendeen;grand bleu de gascogne;grand gascon saintongeois;grand griffon vendeen;great dane;great japanese dog;great pyrenees;greater swiss mountain dog;greek harehound;greek sheepdog;greek shepherd;greenland dog;greenland husky;greyhound;griffon bleu de gascogne;griffon bruxellois;griffon fauve de bretagne;griffon nivernais;groenendael;grosser schweizer sennenhund;gull dong;gull terr;hairless khala;haldenstovare;hamiltonst\xC3\xB6vare;hanover hound;harrier;havanese;himalayan sheepdog;hokkaido;hollandse herder;hovawart;hungarian greyhound;hungarian vizsla;hungarian wirehaired vizsla;huntaway;hygenhund;ibizan hound;icelandic sheepdog;indian bullterrier;indian mastiff;irish bull terrier;irish red and white setter;irish setter;irish staffordshire terrier;irish terrier;irish water spaniel;irish wolfhound;istarski kratkodlaki gonic;istarski ostrodlaki gonic;istrian coarse-haired hound;istrian sheepdog;italian greyhound;italian spinone;jdj american bulldog;jack russell terrier;jagdterrier;japanese chin;japanese spitz;japanese terrier;jindo;jonangi;j\xC3\xA4mthund;kai ken;kangal dog;kanni;karelian bear dog;kars dog;keeshond;kelb-tal fenek;kelpie;kerry blue terrier;king charles spaniel;kishu;kombai;komondor;kooikerhondje;koolie;korea jindo dog;korean mastiff;korthals griffon;krasky ovcar;kromfohrlander;kuvasz;kyi leo;labradoodle;labrador retriever;laekenois;lagotto romagnolo;lakeland terrier;lancashire heeler;landseer;lapinporokoira;large munsterlander;leonberger;lhasa apso;llewellyn setter;l\xC3\xB6wchen;mackenzie river husky;magyar agar;malinois;maltese;manchester terrier;maremma sheepdog;mastiff;meliteo kinidio;mexican hairless;mexican hairless dog;middle asian owtcharka;miniature australian shepherd;miniature bull terrier;miniature fox terrier;miniature pinscher;miniature schnauzer;mioritic;mixed-breed dog;moscovskaya storozhevaya sobaka;moscow watchdog;mountain burmese;mountain cur;mudhol hound;mudi;munsterlander;neapolitan mastiff;nebolish mastiff;newfoundland;norfolk terrier;norrbottenspets;northern inuit;norwegian buhund;norwegian elkhound;norwegian lundehund;norwich terrier;nova scotia duck-tolling retriever;old danish pointer;old english bulldog;old english sheepdog;olde englishe bulldogge;osterreichischer kurzhaariger pinscher;otterhound;otto;owczarek podhalanski;panja;papillon;parson russell terrier;patterdale terrier;pekeapoo;pekingese;pembroke welsh corgi;perdiguero de burgos;perro cimarron;perro de pastor mallorquin;perro de presa canario;perro de presa mallorquin;perro de toro;peruvian hairless dog;peruvian inca orchid;petit basset griffon vendeen;petit bleu de gascogne;petit brabancon;petit gascon saintongeois;phal\xC3\xA8ne;pharaoh hound;phung san;picardy shepherd;picardy spaniel;pinscher;pit bull;plott hound;podenco canario;pointer;poitevin;polish greyhound;polish hound;polish lowland sheepdog;polish ogar;polish scenthound;polish sighthound;polish tatra sheepdog;polski owczarek nizinny;pomeranian;pont-audemer spaniel;poodle;porcelaine;portuguese podengo;portuguese pointer;portuguese shepherd dog;portuguese water dog;posavac hound;prazsky krysavik;pudelpointer;pug;puggle;puli;pumi;pyrenean mastiff;pyrenean mountain dog;pyrenean shepherd;queensland heeler;rafeiro do alentejo;rajapalayam;rampur greyhound;rat terrier;ratonero bodeguero andaluz;redbone coonhound;rhodesian ridgeback;romanian shepherd dog;rottweiler;rough collie;russell terrier;russian black terrier;russian hound;russian spaniel;russian toy terrier;russian tsvetnaya bolonka;russko-evropeiskaia laika;saarlooswolfhond;sabueso espanol;saluki;samoyed;sapsali;sarplaninac;schapendoes;schillerstovare;schipperke;schnauzer ;schnoodle;schweizer laufhund;schweizer niederlaufhund;scott\'s american bulldog;scottish deerhound;scottish terrier;sealyham terrier;segugio italiano;sennenhund/cattle dog;seppala siberian sleddog;serbian hound;serbian mountain hound;serbian tricolour hound;shar pei;shetland sheepdog;shiba inu;shih tzu;shikoku;shiloh shepherd dog;siberian husky;silken windhound;silky terrier;sindh mastiff;skye terrier;sloughi;slovak cuvac;slovakian hound;slovensky hrubosrsty stavac;smalandsstovare;small greek domestic dog;small munsterlander;smooth collie;smooth fox terrier;soft-coated wheaten terrier;south russian ovtcharka;southern white american bulldog;spanish alano;spanish galgo;spanish mastiff;spanish water dog;spinone italiano;spitz;springer spaniel;st. bernard;stabyhoun;staffordshire bull terrier;standard schnauzer;stephens cur;styrian coarse-haired hound;sussex spaniel;swedish elkhound;swedish farm dog;swedish lapphund;swedish vallhund;swiss shorthaired pinscher;tatra shepherd dog;teddy roosevelt terrier;tenterfield terrier;tervueren;tervuren;thai bangkaew dog;thai ridgeback;tibetan kyi apso;tibetan lhasa apso;tibetan mastiff;tibetan spaniel;tibetan terrier;tosa;toy bulldog;toy fox terrier;toy manchester terrier;toy mi-ki;transylvanian hound;treeing cur;treeing tennessee brindle;treeing walker coonhound;tsvetnaya bolonka;tyrolean hound;utonagan;valley bulldog;vizsla;volpino italiano;weimaraner;welsh corgi;welsh springer spaniel;welsh terrier;west highland white terrier;west siberian laika;westphalian dachsbracke;wetterhoun;whippet;white shepherd dog;wilkinson bulldog;wire fox terrier;wirehaired pointing griffon;xoloitzcuintle;yorkshire terrier;yugoslavian mountain hound;yugoslavian tricolour hound;bdutch shepherd dog;bdutch smoushond", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kdummyText_NCM_version, Type = String, Dynamic = False, Default = \"20190926", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kFruit_start, Type = String, Dynamic = False, Default = \"Fruit ipsum dolor sit amet", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kFruit_words, Type = String, Dynamic = False, Default = \"akee;apple;apricot;avocado;avocado;a\xC3\xA7a\xC3\xAD;banana;bell pepper;bilberry;black sapote;blackberry;blackcurrant;blood orange;blueberry;boysenberry;buddha\'s hand;cantaloupe;cherimoya;cherry;chico fruit;chili pepper;clementine;cloudberry;coconut;corn kernel;crab apples;cranberry;cucumber;cucumber;currant;custard apple;damson;date;dragonfruit;durian;eggplant;elderberry;feijoa;fig;fingered citron;goji berry;gooseberry;grape;grapefruit;guava;honeyberry;honeydew;horned melon;huckleberry;jabuticaba;jackfruit;jambul;japanese plum;jostaberry;jujube;juniper berry;kiwano;kiwifruit;kumquat;lemon;lime;longan;loquat;lychee;mamin chino;mandarine;mango;mangosteen;marionberry;melon;miracle fruit;mulberry;nance;nectarine;olive;orange;papaya;passionfruit;pea;peach;pear;persimmon;pineapple;pineberry;pitaya;plantain;plum;plumcot;pluot;pomegranate;pomelo;prune;pumpkin;purple mangosteen;quince;raisin;rambutan ;raspberry;redcurrant;salak;salal berry;salmonberry;satsuma;soursop;squash;star apple;star fruit;strawberry;surinam cherry;tamarillo;tamarind;tangerine;tomato;ugli fruit;watermelon;white currant;white sapote;yuzu;zucchini", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kLoremIpsum_start, Type = String, Dynamic = False, Default = \"Lorem ipsum dolor sit amet", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kLoremIpsum_words, Type = String, Dynamic = False, Default = \"a;ac;accumsan;ad;adipiscing;aenean;aliqua;aliquam;aliquet;aliquip;amet;anim;ante;aptent;arcu;at;auctor;augue;aute;bibendum;blandit;cillum;class;commodo;condimentum;congue;consectetur;consequat;conubia;convallis;cras;cubilia;culpa;cupidatat;curabitur;curae;cursus;dapibus;deserunt;diam;dictum;dictumst;dignissim;dis;do;dolor;dolore;donec;dui;duis;ea;efficitur;egestas;eget;eiusmod;eleifend;elementum;elit;enim;erat;eros;esse;est;et;etiam;eu;euismod;ex;excepteur;exercitation;facilisi;facilisis;fames;faucibus;felis;fermentum;feugiat;finibus;fringilla;fugiat;fusce;gravida;habitant;habitasse;hac;hendrerit;himenaeos;iaculis;id;imperdiet;in;inceptos;incididunt;integer;interdum;ipsum;irure;justo;labore;laboris;laborum;lacinia;lacus;laoreet;lectus;leo;libero;ligula;litora;lobortis;lorem;luctus;maecenas;magna;magnis;malesuada;massa;mattis;mauris;maximus;metus;mi;minim;molestie;mollis;mollit;montes;morbi;mus;nam;nascetur;natoque;nec;neque;netus;nibh;nisi;nisl;non;nostra;nostrud;nulla;nullam;nunc;occaecat;odio;officia;orci;ornare;pariatur;parturient;pellentesque;penatibus;per;pharetra;phasellus;placerat;platea;porta;porttitor;posuere;potenti;praesent;pretium;primis;proident;proin;pulvinar;purus;quam;qui;quis;quisque;reprehenderit;rhoncus;ridiculus;risus;rutrum;sagittis;sapien;scelerisque;sed;sem;semper;senectus;sint;sit;sociosqu;sodales;sollicitudin;sunt;suscipit;suspendisse;taciti;tellus;tempor;tempus;tincidunt;torquent;tortor;tristique;turpis;ullamco;ullamcorper;ultrices;ultricies;urna;ut;varius;vehicula;vel;velit;venenatis;veniam;vestibulum;vitae;vivamus;viverra;voluptate;volutpat;vulputate", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kMeats_start, Type = String, Dynamic = False, Default = \"Meat ipsum dolor sit amet", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kMeats_words, Type = String, Dynamic = False, Default = \"ad;adipisicing;alcatra;aliqua;aliquip;amet;andouille;anim;aute;bacon;ball;beef;belly;biltong;boudin;bresaola;brisket;buffalo;burgdoggen;capicola;chicken;chop;chuck;cillum;consectetur;consequat;corned;cow;culpa;cupim;deserunt;dolor;dolore;doner;drumstick;duis;eiusmod;elit;enim;est;eu;excepteur;fatback;filet;flank;frankfurter;fugiat;ground;ham;hamburger;hock;id;in;incididunt;ipsum;irure;jalapeno;jerky;jowl;kevin;kielbasa;labore;laboris;laborum;landjaeger;leberkas;loin;lorem;magna;meatball;meatloaf;mignon;minim;mollit;nisi;non;nulla;occaecat;officia;pancetta;pariatur;pastrami;picanha;pig;porchetta;pork;proident;prosciutto;qui;quis;reprehenderit;ribeye;ribs;round;rump;salami;sausage;sed;shank;shankle;short;shoulder;sint;sirloin;spare;spicy;steak;strip;sunt;swine;tbone;tail;tempor;tenderloin;tip;tongue;tritip;turducken;turkey;ullamco;ut;veniam;venison;voluptate", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kSeafood_start, Type = String, Dynamic = False, Default = \"Seafood ipsum dolor sit amet", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kSeafood_words, Type = String, Dynamic = False, Default = \"abalone;alaska pollock;albacore tuna;american lobster;american shad;anchovy;herring;arctic char;atlantic mackerel;atlantic ocean perch;atlantic salmon;barracuda;barramundi;basa;bay scallop;black sea bass;black tiger shrimp;blue crab;blue mussel;blue\xEF\xAC\x81n tuna;bream;carp;cat\xEF\xAC\x81sh;chilean sea bass;chinese white shrimp;chinook salmon;chum salmon;clam;cobia;cockle;cod;coho salmon;conch;crab;cray\xEF\xAC\x81sh;croaker;cusk;dog\xEF\xAC\x81sh;cuttle\xEF\xAC\x81sh;dory;drum;dungeness crab;eastern oyster;eel;blue marlin;escolar;european oyster;european sea bass;\xEF\xAC\x82ounder;freshwater shrimp;geoduck clam;greenshell mussel;grouper;gulf shrimp;haddock;hake;halibut;hardshell clam;hardshell clam;hoki;hybrid striped bass;jellyfish;jonah crab;king crab;kingklip;krill;lake victoria perch;lake white\xEF\xAC\x81sh;lingcod;moi;lobster;mahimahi;mako shark;monk\xEF\xAC\x81sh;mullet;octopus;opah;orange roughy;paci\xEF\xAC\x81c oyster;paci\xEF\xAC\x81c white shrimp;peekytoe crab;pink salmon;pink shrimp;pompano;dover sole;rainbow trout;rock shrimp;rock\xEF\xAC\x81sh;sable\xEF\xAC\x81sh;scup;sea scallop;sea urchin;shrimp;skate;smelt;snapper;snow crab;sockeye salmon;spanner crab;spiny lobster;squat lobster;squid;stone crab;sturgeon;surf clam;sword\xEF\xAC\x81sh;tilapia;tile\xEF\xAC\x81sh;turbot;wahoo;walleye;wolf\xEF\xAC\x81sh;yellow perch;yellow\xEF\xAC\x81n tuna;yellowtail", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kVegetables_start, Type = String, Dynamic = False, Default = \"Vegetables ipsum dolor sit amet", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kVegetables_words, Type = String, Dynamic = False, Default = \"amaranth leaves;artichoke;arugula;aubergine;avocado;baby corn;bamboo shoots;bean sprouts;beans;beet;beet greens;belgian endive;bell pepper;bibb lettuce;bitter gourd;bitter melon;bok choi;bok choy;boston lettuce;broccoli;brussels sprouts;burdock root;butterhead lettuce;calabash;candle corn;capers;carrot;cassava;cauliflower;celeriac;celery;celery root;celtuce;chayote;chinese broccoli;chinese spinach;collard greens;corn;cucumber;curly endive;curly kale;daikon radish;dandelion greens;edamame;eggplant;elephant garlic;english cucumber;escarole endive;fennel;fiddlehead;frisee endive;galangal;garlic;gherkin cucumber;ginger;gobo;grape leaves;green asparagus;green beans;green cabbage;green leaf lettuce;green onions;greens;hearts of palm;horseradish;iceberg lettuce;jerusalem artichoke;j\xC3\xADcama;kai-lan;kale;kale;kohlrabi;kohlrabi greens;lacinato kale;leeks;lemongrass;lettuce;lotus root;lotus seed;maize;mushrooms;mustard greens;napa cabbage;nopales;okra;olive;onion;ornamental kale;pak choy;parsley;parsley root;parsnip;peas;peppers;pickling cucumbers;plantain;potato;pumpkin;purple asparagus;purslane;radicchio;radish;rapini;red cabbage;red leaf lettuce;romaine lettuce;rutabaga;savoy cabbage;scallions;shallots;snap beans;spinach;spinach;squash;string beans;sunchokes;sweet potato;swiss chard;swiss chard;taro;tomatillo;tomato;turnip;turnip greens;water chestnut;water spinach;watercress;wax beans;white asparagus;winter melon;yams;yuca;zucchini;green peas;snow peas;sugar snap peas", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
