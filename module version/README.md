# READ ME #

## dummyText_NCM module ##

the dummyText_NCM module is designed to generate various Lorem Ipsum style text.  you can either generate words, sentences or paragraphs.  below is a list of the dummy texts we can generate.  more to come soon(tm).

* Lorem Ipsum
* Meats

### testing ###
currently there is no testing.  testing will come soon(tm).

### optimization ###
currently the methods are not optimized.  All methods are using the first try to complete the method over using the best method.  Optimizations will come in the future.

### sample code ###
there is a sample program that generates dummy text using this module.

### version history ###
* 2018-02-24 - initial commit (for public sharing).
